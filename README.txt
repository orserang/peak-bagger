MCMC method:

  Run from directory peak-bagger/src/mcmc/

  BUILD:

    Using "make".

    Set the number of threads at compile-time by building with "make
    NUMTHREADS=<number of threads>".  If NUMTHREADS is not specified,
    the default value is 32.
  

  RUN USING:

    ./solve <alphabet size> <seed file> <epsilon> <intensity_cutoff>
    <theta> <whitelist file> <iterations per epoch> <epochs> [-f:
    Protect seeds] [-r <rank output file>: rank deltas] <mgf> [mgf]...

  PARAMETERS:

    alphabet size: Number of masses allowed in the alphabet.  Set at the
    command line and constant through-out the program.
    
    seed file: File with seed deltas (deltas which will be included in
    the first iteration of the alphabet) with one mass/label per line.
    Of the form:
    12.00 Carbon
    18.01 Water	
    
    epsilon: fragmentation ion tolerance in Daltons.  Ideally, value
    should be machine-dependent.
    
    intensity cutoff: Intensity threshhold relative to largest intensity
    peak.  Any peak with intensity less than the intensity cutoff will
    be removed from spectra. (i.e. 0.01 removes bottom 1\% of peaks).
    
    theta: Parameter used to adjust likelihood of accepting new alphabet
    
    whitelist file: File of known masses and labels used to check the
    alphabet.  Same file format as the seed file.
    
    iterations: Amount of iterations in an epoch for the Gibbs sampler.
    One iteration means one swapped out alphabet mass.
    
    epochs: Number of epochs.  After each epoch the all threads are
    seeded with the best alphabet form all threads.
    
    mgf: MGF file with spectra.  of the form
    BEGIN IONS
    <Other spectra information, must start with alphabet character>
    <m/z> <intensity>
    <m/z> <intensity>
    <m/z> <intensity>
    ...
    END IONS
    
    e.g.
    BEGIN IONS
    TITLE=spectrum title
    PRECMASS=19991.03Da
    112.13  1002.31
    125.34  930.32
    END IONS
    
    -f: used to protect the seed deltas.  This means if a seed file is
     used then any deltas from it are not allowed to be removed from the
     alphabet.
    
    -r <rank output file>: Use to output a ranking of the masses used in
     the alphabet.  Each mass which has ever been in an alphabet is output
     into the file along with the amount of alphabets it was apart of.

