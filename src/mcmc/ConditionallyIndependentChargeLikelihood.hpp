#ifndef _CONDITIONALLYINDEPENDENTCHARGELIKELIHOOD_HPP
#define _CONDITIONALLYINDEPENDENTCHARGELIKELIHOOD_HPP

#include "Likelihood.hpp"

class ConditionallyIndependentChargeLikelihood : public Likelihood {
protected:
  virtual double get_log_likelihood_of_single_connected_graph(const Spectrum *spect, const PrimitiveVector<Edge> & subgraph) = 0;

  virtual double get_log_likelihood_of_charge_state_graphs_on_single_spectrum(const Spectrum *spect, const double* alphabet, int* buffer, const int alphabet_size, const double epsilon, const int max_charge, const std::unordered_map<int, std::vector<PrimitiveVector<Edge> > > & charge_to_connected_graphs) override {
    double log_likelihood = 0.0;

    for (const auto & p : charge_to_connected_graphs) {
      const std::vector<PrimitiveVector<Edge> > & connected_subgraphs = p.second;
      
      if (connected_subgraphs.size() > 0) {
	double log_likelihood_for_charge = log(0.0);
	for (const PrimitiveVector<Edge> &connected_subgraph : connected_subgraphs) {
	  double graph_log_likelihood = get_log_likelihood_of_single_connected_graph(spect, connected_subgraph);
	  log_likelihood_for_charge = log_sum(log_likelihood_for_charge, graph_log_likelihood);
	}
	
	log_likelihood += log_likelihood_for_charge;
      }
    }
    return log_likelihood;
  }
};

#endif
