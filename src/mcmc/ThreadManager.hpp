#ifndef _THREADMANAGER_HPP
#define _THREADMANAGER_HPP

#include "Sampler.hpp"
#include "AlphabetEvaluator.hpp"
#include "DeltaProposer.hpp"

#include <algorithm>

class ThreadManager {
private:
  std::vector<std::vector<Graph> > _graphs;
  std::vector<Sampler*> _samplers;
  AlphabetEvaluator _alphabet_evaluator;
  const double _epsilon;
  double       _best_llh;
  double       _best_acceptance_rate;

  double*      _best_alphabet;
  const int    _alphabet_size;
  long         _epochs_ran;
  long         _total_iterations_ran;
  long         _number_of_accepted;

  int          _best_thread;
  std::vector<std::unordered_map<double, int> > _vector_of_threads_delta_to_frequency_in_alphabet;
  const bool _rank_deltas;
  const char* _rank_deltas_out_file;

  int max_spectrum_size(const std::vector<Spectrum> &spectra){
    int max_size = -1;
    for (const Spectrum & s : spectra)
      if (s.number_peaks() > max_size)
	max_size = s.number_peaks();
    return max_size;
  }

  void spawn_threads(const std::vector<Spectrum> &spectra, const std::vector<double> &seed_deltas, const int max_number_peaks, const int number_protected_seeds, const int max_charge, const double theta, const bool rank_deltas, const char* rank_deltas_out_file){
    _samplers.resize(NUMTHREADS);
    // fixme: verify that this can be safely multithreaded (it looks fine, but good to check)
    #pragma omp parallel for
    for (int i=0; i<NUMTHREADS; ++i) {
      DeltaProposer*proposer_ptr = new DeltaProposer(spectra, _epsilon, _alphabet_size, max_charge, number_protected_seeds);
      _samplers[i] = new Sampler(spectra, seed_deltas, proposer_ptr, _epsilon, _alphabet_size, max_number_peaks, number_protected_seeds, max_charge, theta, new Model, _vector_of_threads_delta_to_frequency_in_alphabet[i], rank_deltas, rank_deltas_out_file);
    }
  }

  void set_alphabets(){
    for (int i=0; i<NUMTHREADS; ++i)
      _samplers[i]->set_alphabet(_best_alphabet);
  }

public:
  ThreadManager(AlphabetEvaluator & ae, const std::vector<Spectrum> &spectra, const std::vector<double> &seed_deltas, const int alphabet_size, const double epsilon, const int number_protected_seeds, const double theta, const int max_charge, const bool rank_deltas, const char* rank_deltas_out_file):
    _alphabet_evaluator(ae),
    _epsilon(epsilon),
    _best_llh(log(0.0)),
    _best_acceptance_rate(0.0),
    _alphabet_size(alphabet_size),
    _epochs_ran(0),
    _total_iterations_ran(0),
    _number_of_accepted(0),
    _vector_of_threads_delta_to_frequency_in_alphabet(NUMTHREADS),
    _rank_deltas(rank_deltas),
    _rank_deltas_out_file(rank_deltas_out_file)
  {
    _best_alphabet = new double[alphabet_size];
    int max_number_peaks = max_spectrum_size(spectra);
    spawn_threads(spectra, seed_deltas, max_number_peaks, number_protected_seeds, max_charge, theta, rank_deltas, rank_deltas_out_file);
  }

  ~ThreadManager(){
    delete[] _best_alphabet;
    for (Sampler*s : _samplers)
      delete s;
  }

  double* best_alphabet(){
    return _best_alphabet;
  }

  double best_likelihood(){
    return _best_llh;
  }

  double best_acceptance_rate(){
    return _best_acceptance_rate;
  }

  void set_best_alphabet() {
    _best_llh = _samplers[0]->likelihood();
    _best_thread = 0;
    for (int i=1; i<NUMTHREADS; ++i)
      if (_best_llh <  _samplers[i]->likelihood()){
	_best_llh =  _samplers[i]->likelihood();
	_best_thread = i;
      }

    _best_alphabet = _samplers[_best_thread]->alphabet();
    _best_acceptance_rate = _samplers[_best_thread]->overall_acceptance_rate();
  }

  void run(const int iterations, const int epochs){
    for (int epoch_ind = 0; epoch_ind < epochs; ++epoch_ind){
      #pragma omp parallel for
      for (int i=0; i<NUMTHREADS; ++i)
	_samplers[i]->run(iterations);

      set_best_alphabet();

      std::cout << "Overall acceptance rate: " << _best_acceptance_rate << std::endl;
      std::cout << "Epoch acceptance rate: " << _samplers[_best_thread]->epoch_acceptance_rate() << std::endl;
      std::cout << "Overall avg. difference between proposed and current state's log likelihoods: " << _samplers[_best_thread]->avg_difference_between_log_llh() << std::endl;

      // Sort alphabet for printing
      std::sort(_best_alphabet, _best_alphabet+_alphabet_size);

      // fixme: do this multiple times:
      // copy the best thread's result into every other thread:
      set_alphabets();

      _alphabet_evaluator.print_stats(_best_alphabet, _alphabet_size);
    }
    if (_rank_deltas){
      std::cout << "Outputing to delta rank file" << std::endl;
      std::ofstream outfile;
      outfile.open(_rank_deltas_out_file);

      for (int i = 0; i < NUMTHREADS; ++i){
	outfile << "Thread " << i << "\n";
	for (auto it = _vector_of_threads_delta_to_frequency_in_alphabet[i].begin(); it!= _vector_of_threads_delta_to_frequency_in_alphabet[i].end(); ++it){
	  outfile << it->first << " " << it->second << "\n";
	}
      }
      outfile.close();      
    }
  }
};
#endif
