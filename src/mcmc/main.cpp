#include <iostream>
#include <fstream>
#include <string>
#include <chrono>
#include <vector>

// fixme:
constexpr int MAX_CHARGE=3;

#include "PeakLikelihood.hpp"
#include "EdgeLikelihood.hpp"
#include "UniquePeakLikelihood.hpp"
#include "UniqueEdgeLikelihood.hpp"
#include "GraphIsomorphismLikelihood.hpp"
//#include "EdgeGraphIsomorphismLikelihood.hpp"
//#include "PeakGraphIsomorphismLikelihood.hpp"
//typedef GraphIsomorphismLikelihood Model;
//typedef UniqueEdgeLikelihood Model;
//typedef UniquePeakLikelihood Model;
typedef EdgeLikelihood Model;
//typedef PeakLikelihood Model;
//typedef EdgeGraphIsomorphismLikelihood Model;
//typedef PeakGraphIsomorphismLikelihood Model;

#include "Spectrum.hpp"
#include "Graph.hpp"
#include "ThreadManager.hpp"
#include "AlphabetEvaluator.hpp"
#include "Likelihood.hpp"

void get_alphabet_file(const char*seed_delta_file, std::vector<double> &seeded_deltas, const char* whitelist_file, std::vector<std::pair<std::string, double> > &whitelist){
  std::ifstream seedfile(seed_delta_file, std::ifstream::binary);
  std::string line;
  while(getline(seedfile,line)){
    double mass;
    std::istringstream ist(line);
    if (! (ist >> mass) )
      break;
    seeded_deltas.push_back(mass);
  }

  std::ifstream fin(whitelist_file);
  while (getline(fin, line)) {
    if (line.empty() != 1){
      std::istringstream ist(line);
      double mass;
      std::string name;
      ist >> mass >> name;
      whitelist.push_back(std::make_pair(name, mass));
    }
  }
}

std::vector<Spectrum> get_spectra(int argc, char* argv[], int spectra_start, double epsilon, double TABLE_EPSILON, double intensity_cutoff){
  std::vector<Spectrum> spectra;
  std::cout << "Spectra...\n";
  for (int i=spectra_start; i<argc; ++i) {
    std::cout << "\t" << argv[i] << std::endl;
    std::vector<Spectrum> file_spectra = load_mgf(argv[i], epsilon, TABLE_EPSILON, intensity_cutoff);
    for (const Spectrum & s : file_spectra)
      spectra.push_back(s);
  }
  std::cout << std::endl;
  return spectra;
}


void print_stats_for_seeded_deltas(AlphabetEvaluator &ae, const std::vector<double> &seeded_deltas){
  double* alphabet = (double*) malloc(seeded_deltas.size() * sizeof(double));
  for (unsigned int i=0; i<seeded_deltas.size(); ++i)
    alphabet[i] = seeded_deltas[i];
  std::cout << "Alphabet before chaning to canonical mass..." << std::endl;

  ae.print_stats(alphabet, seeded_deltas.size());
  ae.canon(alphabet, seeded_deltas.size());
  //ae.public_change_to_most_connected_charge_states(alphabet, seeded_deltas.size());
  std::cout << "Alphabet after chaning to canonical mass..." << std::endl;
  ae.print_simple_annotated_alphabet(alphabet, seeded_deltas.size());

  free(alphabet);
}

void run_metropolis_hastings(AlphabetEvaluator &ae, const std::vector<Spectrum> &spectra, const std::vector<double> &seeded_deltas, const std::vector<std::pair<std::string, double> > &whitelist, const int alphabet_size, const int iterations, const int epochs, const double epsilon, const int number_protected_seeds, const double theta, const bool rank_deltas, const char* rank_deltas_out_file){
  ThreadManager thread_manager(ae, spectra, seeded_deltas, alphabet_size, epsilon, number_protected_seeds, theta, MAX_CHARGE, rank_deltas, rank_deltas_out_file);
  thread_manager.run(iterations, epochs);
  Clock c;
  double* alphabet = thread_manager.best_alphabet();
  // sort  alphabet
  for (int i=0; i<alphabet_size; ++i){
    int j=i;
    while(j > 0 && alphabet[j-1] > alphabet[j]){
      double tmp = alphabet[j];
      alphabet[j] = alphabet[j-1];
      alphabet[j-1] = tmp;
        --j;
    }
  }
  
  /* Print Annotated Alphabet */
  std::cout << "Likelihood: " << thread_manager.best_likelihood() << std::endl;
  std::cout << "Acceptance rate: " << thread_manager.best_acceptance_rate() << std::endl;
  ae.print_simple_annotated_alphabet(alphabet, alphabet_size);
}

int main(int argc, char* argv[]){
  // From StackOverflow:
  auto now = std::chrono::system_clock::now();
  auto now_ms = std::chrono::time_point_cast<std::chrono::milliseconds>(now);
  auto value = now_ms.time_since_epoch();
  long seed = value.count();
  std::cout << "Seed: " << seed << std::endl;
  srand(seed);
  constexpr double TABLE_EPSILON=0.1;
  if (argc < 9){
    std::cerr << "USAGE: <alphabet size> <seed file> <epsilon> <intensity_cutoff> <theta> <whitelist file> <iterations per epoch> <epochs> [-f: Protect seeds] [-r <output file>: rank deltas] <mgf> [mgf]...\n";
    exit(1);
  }

  int alphabet_size       = atoi(argv[1]);
  char*seed_delta_file    = argv[2];
  double epsilon          = atof(argv[3]);
  double intensity_cutoff = atof(argv[4]);
  double theta            = atof(argv[5]);
  char*whitelist_file     = argv[6];
  int iterations          = atoi(argv[7]);
  int epochs              = atoi(argv[8]);
  int optional_arguments_start = 9;
  int spectra_start       = optional_arguments_start;
  int number_protected_seeds = 0;
  bool rank_deltas        = false;
  char* rank_deltas_out_file;

  std::cout << "Alphabet size " << alphabet_size << std::endl;
  std::cout << "Seed delta file " << seed_delta_file << std::endl;
  std::cout << "Epsilon " << epsilon << std::endl;
  std::cout << "Theta " << theta << std::endl;  
  std::cout << "Whitelist file " << whitelist_file << std::endl;
  std::cout << "Intenisty cutoff " << intensity_cutoff << std::endl;
  std::cout << "Iterations per epoch " << iterations << std::endl;
  std::cout << "Number of epochs " << epochs << std::endl;
  std::cout << "Number of threads " << NUMTHREADS << std::endl;
  std::cout << "Number arguments " << argc << std::endl;

  /* Read in alphabet file */
  std::vector<double> seeded_deltas;
  std::vector<std::pair<std::string, double> > whitelist(0);
  get_alphabet_file(seed_delta_file, seeded_deltas, whitelist_file, whitelist);
  if (seeded_deltas.size()  > (unsigned)alphabet_size)
    std::cout << "WARNING: Number of seeded deltas is larger than the alphabet size, will only use the first " << alphabet_size << " seeded deltas." << std::endl;

  /* FIND OPTIONAL FLAGS */
  if (strcmp(argv[optional_arguments_start], "-f") == 0){
    std::cout << "Protecting seeds: " << "true" << std::endl;
    ++spectra_start;
    number_protected_seeds = seeded_deltas.size();
  }
  else
    std::cout << "Protecting seeds: " << "false" << std::endl;

  for (int i = optional_arguments_start; i < argc; ++i){
    if (strcmp(argv[i], "-r") == 0){

      spectra_start += 2;
      rank_deltas_out_file = argv[i+1];
      rank_deltas = true;
      std::cout << "Writing delta ranks to file: " << rank_deltas_out_file << std::endl;
    }
  }

  if (!rank_deltas)
    std::cout << "Writing delta ranks: " << "false" << std::endl;

  std::cout << "Number of protected seeds " << number_protected_seeds << std::endl;

  /* Read in data files and create spectrum objects */
  std::vector<Spectrum> spectra = get_spectra(argc, argv, spectra_start, epsilon, TABLE_EPSILON, intensity_cutoff);

  std::cout << "Loaded " << spectra.size() << " spectra" << std::endl;
  
  AlphabetEvaluator alphabet_evaluator(spectra, whitelist, epsilon, MAX_CHARGE, new Model);
  if (epochs >0 && iterations >0 && (number_protected_seeds < alphabet_size) ){
    run_metropolis_hastings(alphabet_evaluator, spectra, seeded_deltas, whitelist, alphabet_size, iterations, epochs, epsilon, number_protected_seeds, theta, rank_deltas, rank_deltas_out_file);
  }
  else
    print_stats_for_seeded_deltas(alphabet_evaluator, seeded_deltas);

  return 0;
}
