#ifndef _UNIQUEPEAKLIKELIHOOD_HPP
#define _UNIQUEPEAKLIKELIHOOD_HPP

#include "Likelihood.hpp"

#include "PairHash.hpp"
#include <unordered_set>

class UniquePeakLikelihood : public Likelihood {
protected:
  virtual double get_log_likelihood_of_charge_state_graphs_on_single_spectrum(const Spectrum *spect, const double* alphabet, int* buffer, const int alphabet_size, const double epsilon, const int max_charge, const std::unordered_map<int, std::vector<PrimitiveVector<Edge> > > & charge_to_connected_graphs) override {
    std::unordered_set<int> peaks_visited;

    double log_likelihood = 0.0;

    for (const auto & p : charge_to_connected_graphs) {
      const std::vector<PrimitiveVector<Edge> > & connected_subgraphs = p.second;
      
      double log_likelihood_for_charge = log(0.0);
      for (const PrimitiveVector<Edge> &connected_subgraph : connected_subgraphs) {
	double graph_log_likelihood = get_log_likelihood_of_single_connected_graph(spect, connected_subgraph, peaks_visited);
	log_likelihood_for_charge = log_sum(log_likelihood_for_charge, graph_log_likelihood);
      }
	
      log_likelihood += log_likelihood_for_charge;
    }
    return log_likelihood;
  }

  double get_log_likelihood_of_single_connected_graph(const Spectrum *spect, const PrimitiveVector<Edge> & subgraph, std::unordered_set<int> & peaks_visited) {
    double llh = 0;
    for (unsigned int i=0; i < subgraph.size(); ++i){
      if (peaks_visited.find(subgraph[i].start_peak_index) == peaks_visited.end())
	llh += spect->get_log_intensity(subgraph[i].start_peak_index);
      if (peaks_visited.find(subgraph[i].end_peak_index) == peaks_visited.end())
	llh += spect->get_log_intensity(subgraph[i].end_peak_index);

      // Note: peaks are claimed by the smallest charge state first
      peaks_visited.insert(subgraph[i].start_peak_index);
      peaks_visited.insert(subgraph[i].end_peak_index);
    }
    return llh;
  }
  
public:
  UniquePeakLikelihood(){
    std::cout << "Using llh model UniquePeakLikelihood " << std::endl;
  }
};

#endif
