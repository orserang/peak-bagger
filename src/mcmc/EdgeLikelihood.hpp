#ifndef _EDGELIKELIHOOD_HPP
#define _EDGELIKELIHOOD_HPP

#include "ConditionallyIndependentChargeLikelihood.hpp"

#include "PairHash.hpp"
#include <unordered_set>

class EdgeLikelihood : public ConditionallyIndependentChargeLikelihood {
protected:
  double get_log_likelihood_of_single_connected_graph(const Spectrum *spect, const PrimitiveVector<Edge> & subgraph) override {
    std::unordered_set<std::pair<int, int>, PairHash<int,int> > edges_visited;

    double llh = 0;
    for (unsigned int i=0; i < subgraph.size(); ++i) {
      std::pair<int,int> p(subgraph[i].start_peak_index, subgraph[i].end_peak_index);

      // Do not mark the same edge more than once:
      if (edges_visited.find(p) == edges_visited.end()) {
	llh += spect->get_log_intensity(p.first);
	llh += spect->get_log_intensity(p.second);

	edges_visited.insert(p);
      }
    }
    return llh;
  }

public:
  EdgeLikelihood(){
    std::cout << "Using llh model EdgeLikelihood " << std::endl;
  }
};

#endif
