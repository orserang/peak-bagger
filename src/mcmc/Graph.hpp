#ifndef _GRAPH_HPP
#define _GRAPH_HPP

#include "Spectrum.hpp"
#include "Clock.hpp"
#include "PrimitiveVector.hpp"

struct Edge{
  int start_peak_index;
  int end_peak_index;
  int delta_index;
};

class Graph{
private:
  const int _number_peaks;
  PrimitiveVector<Edge> _all_edges_in_graph;
  std::vector<PrimitiveVector<int> > _peak_to_neighbor_indices;
  
  void connect_subgraph_from_start_node(PrimitiveVector<int> &peak_to_subgraph_number, int node, const int new_graph_number, int* buffer){
    int*__restrict stack = (int*)buffer;
    int*__restrict initial_stack=stack;
    bool* __restrict visited = (bool*)(stack+_number_peaks);
    memset(visited, 0, _number_peaks*sizeof(bool));
    *stack = node;
    visited[node] = true;
    while(stack >= initial_stack){
      node = *stack;
      --stack;
      peak_to_subgraph_number[node] = new_graph_number;
  	
      for(unsigned int i=0; i< _peak_to_neighbor_indices[node].size(); ++i){
        int new_node = _peak_to_neighbor_indices[node][i];
        if(!visited[new_node]){
          ++stack;
          *stack = new_node;
          visited[new_node] = 1;
        }
      }
    }
  }

  std::pair<int, PrimitiveVector<int> > mark_peaks_by_connected_subgraph_number(int* buffer){
    PrimitiveVector<int> peak_to_subgraph_number(_number_peaks);
    for (int i=0; i<_number_peaks; ++i)
      peak_to_subgraph_number[i] = -1;
    
    int connected_graph_number = 0;
    for (int i=0; i<_number_peaks; ++i)
      if (peak_to_subgraph_number[i] == -1 && _peak_to_neighbor_indices[i].size() > 0){
	connect_subgraph_from_start_node(peak_to_subgraph_number, i, connected_graph_number, buffer);
	++connected_graph_number;
      }
    return std::make_pair(connected_graph_number, peak_to_subgraph_number);
  }
  
  void connect_peaks_by_alphabet(const Spectrum* spect, const double* alphabet, const int alphabet_size, const double epsilon, const int charge){
    for (int i=0; i<_number_peaks; ++i){
      double start_mz = spect->get_mz(i);
      for (int k=0; k<alphabet_size; ++k){
        double delta = alphabet[k];
	double end_mz = (start_mz + delta/charge);

	int j = spect->find_most_intense_peak_within_epsilon_binned(end_mz, epsilon);
	
	// Do not allow self edges: i != j
	if (j != -1 && i != j) {
	  _all_edges_in_graph.push_back({i,j,k});
	  _peak_to_neighbor_indices[i].push_back(j);
	  _peak_to_neighbor_indices[j].push_back(i);
	}
      }
    }
  }
  
public:
  Graph(const Spectrum* s, const double* alphabet, const int alphabet_size, const double epsilon, const int charge):
    _number_peaks(s->number_peaks()),
    _peak_to_neighbor_indices(s->number_peaks())
  {
    connect_peaks_by_alphabet(s, alphabet, alphabet_size, epsilon, charge);
  }

  // todo: use buffer or change to primitivevector
  std::vector<PrimitiveVector<Edge> > get_connected_subgraphs(int* buffer, const int charge){
    std::pair<int, PrimitiveVector<int> > max_cgn_and_peak_to_subgraph_number = mark_peaks_by_connected_subgraph_number(buffer);
    std::vector<PrimitiveVector<Edge> > connected_graphs(max_cgn_and_peak_to_subgraph_number.first);
    //for (const Edge &e : _all_edges_in_graph)
    for (unsigned int i=0; i < _all_edges_in_graph.size(); ++i)
      connected_graphs[max_cgn_and_peak_to_subgraph_number.second[_all_edges_in_graph[i].start_peak_index]].push_back(_all_edges_in_graph[i]);
    return  connected_graphs;
  }
};
#endif
