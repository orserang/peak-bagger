#ifndef _SAMPLER_HPP
#define _SAMPLER_HPP

#include <float.h>
#include <omp.h>
#include <random>
#include "Likelihood.hpp"
#include "DeltaProposer.hpp"

class Sampler{
private:
  const std::vector<Spectrum>* _spectra_ptr;
  const DeltaProposer* _delta_proposer;
  const double _epsilon;
  const double _theta;
  double _current_log_likelihood;
  double _difference_between_log_llh;
  double _accepted_rate_in_epoch;
  double* _alphabet;
  const int _alphabet_size;
  const int _number_protected_seeds;
  int _number_iterations;
  const int _max_charge;
  int _number_accepted;
  int _number_accepted_in_epoch;
  int* _buffer;
  Likelihood*_model;
  std::unordered_map<double, int>& _delta_to_frequency_in_alphabet;
  const bool _rank_deltas;
  const char* _rank_deltas_out_file;

  void initiate_alphabet(const int number_seeded_deltas){
    for (int i = number_seeded_deltas; i < _alphabet_size; ++i){
      _alphabet[i] = _delta_proposer->propose_initial_delta(_alphabet, _buffer, i).second;
      if(_rank_deltas)
	_delta_to_frequency_in_alphabet[_alphabet[i]] = 1;
    }    
  }

  void set_seeded_deltas(const std::vector<double> &seed_deltas, const int alphabet_size){
    int number_seeded_deltas = seed_deltas.size();
    number_seeded_deltas = std::min(number_seeded_deltas, alphabet_size);
    for (int i=0; i<number_seeded_deltas; ++i)
      _alphabet[i] = seed_deltas[i];

    for (int i=number_seeded_deltas; i<alphabet_size; ++i)
      _alphabet[i] = 0.0;
  }

  bool accept_proposal(std::mt19937 &e2, std::uniform_real_distribution<> &dist){
    double log_likelihood = _model->get_log_likelihood_for_all_spectra(_alphabet, _spectra_ptr, _buffer, _alphabet_size, _epsilon, _max_charge);
    double prob = exp( _theta * (log_likelihood - _current_log_likelihood) );
    _difference_between_log_llh += (log_likelihood - _current_log_likelihood);
    double p = dist(e2);
    
    if (p > prob) // reject
      return false;
    else {
      _current_log_likelihood = log_likelihood;
      ++_number_accepted;
      ++_number_accepted_in_epoch;
      return true;
    }
  }

  
public:
  Sampler(const std::vector<Spectrum> &spectra, const std::vector<double> &seed_deltas, const DeltaProposer* delta_proposer, const double epsilon, const int alphabet_size, const int max_spectrum_size, const int number_protected_seeds, const int max_charge, const double theta, Likelihood*model, std::unordered_map<double, int> &delta_to_frequency_in_alphabet, const bool rank_deltas, const char* rank_deltas_out_file):
    _spectra_ptr(&spectra),
    _delta_proposer(delta_proposer),
    _epsilon(epsilon),
    _theta(theta),
    _current_log_likelihood(0.0),
    _difference_between_log_llh(0.0),
    _accepted_rate_in_epoch(0.0),
    _alphabet_size(alphabet_size),
    _number_protected_seeds(number_protected_seeds),
    _number_iterations(0),
    _max_charge(max_charge),
    _number_accepted(0),
    _number_accepted_in_epoch(0),
    _model(model),
    _delta_to_frequency_in_alphabet(delta_to_frequency_in_alphabet),
    _rank_deltas(rank_deltas),
    _rank_deltas_out_file(rank_deltas_out_file)    
  {
    _alphabet = new double[alphabet_size];
    _buffer = (int*) malloc(max_spectrum_size * sizeof(int) + max_spectrum_size * sizeof(int));
    if (_buffer == NULL) {
      std::cout << "Cannot allocate buffer in Sampler.hpp, intended for use in connecting graphs in the Graph class" << std::endl;
      exit(1);
    }
    set_seeded_deltas(seed_deltas, alphabet_size);
    initiate_alphabet(seed_deltas.size());
  }

  ~Sampler() {
    free(_buffer);
    delete _delta_proposer;
  }

  void run(const int iterations){
    double*old_alphabet = new double[_alphabet_size];

    Clock c;
    std::random_device rd;
    std::mt19937 e2(rd());
    std::uniform_real_distribution<> dist(0, 1);
    _current_log_likelihood = _model->get_log_likelihood_for_all_spectra(_alphabet, _spectra_ptr, _buffer, _alphabet_size, _epsilon, _max_charge);

    _number_accepted_in_epoch = 0;
    for (int i = 0; i < iterations; ++i){
      /* Set proposed delta */
      ++_number_iterations;
      std::pair<int, double> prop_delta = _delta_proposer->propose_delta(_alphabet, _buffer, _alphabet_size);
      double old_delta = _alphabet[prop_delta.first];
      _alphabet[prop_delta.first] = prop_delta.second;

      /* Calculate likelihood and accept or reject proposal */
      if (!accept_proposal(e2, dist)){
	_alphabet[prop_delta.first] = old_delta;
	// Rejected so all alphabet values += 1 in frequency
	if (_rank_deltas)
	  for (int j = 0; j < _alphabet_size; ++j)
	    _delta_to_frequency_in_alphabet[_alphabet[j]] += 1;
      }else {
	// Accepted so all alphabet values but 1 += 1 in frequency
	// Check the other alphabet value to see if already in dictionary
	if (_rank_deltas){
	  for (int j = 0; j < prop_delta.first; ++j)
	    _delta_to_frequency_in_alphabet[_alphabet[j]] += 1;

	  for (int j = prop_delta.first + 1; j < _alphabet_size; ++j)
	    _delta_to_frequency_in_alphabet[_alphabet[j]] += 1;
	  
	  std::unordered_map<double, int>::const_iterator got = _delta_to_frequency_in_alphabet.find (_alphabet[prop_delta.first]);
	  if ( got == _delta_to_frequency_in_alphabet.end() )
	    _delta_to_frequency_in_alphabet[_alphabet[prop_delta.first]] = 1;
	  else
	    _delta_to_frequency_in_alphabet[_alphabet[prop_delta.first]] += 1;
	}
      }
    }
    
    _accepted_rate_in_epoch = (double)_number_accepted_in_epoch / iterations;
    std::cout << "Epoch ";
    c.ptock();
    delete[] old_alphabet;
  }
  
  double* alphabet() const{
    return _alphabet;
  }

  double likelihood() const {
    return _current_log_likelihood;
  }

  double avg_difference_between_log_llh() const {
    return (double)_difference_between_log_llh / _number_iterations;
  }

  double overall_acceptance_rate() const {
    return (double)_number_accepted / _number_iterations;
  }

  double epoch_acceptance_rate() const {
    return _accepted_rate_in_epoch;
  }

  void set_alphabet(double* new_alphabet){
    for (int i=0; i<_alphabet_size; ++i)
      _alphabet[i] = new_alphabet[i];
  }

};
#endif
