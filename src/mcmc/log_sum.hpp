#ifndef _LOG_SUM_HPP
#define _LOG_SUM_HPP

#include <cmath>
#include <limits>

double log_sum(const double log_a, const double log_b){
  if (log_b == -std::numeric_limits<double>::infinity())
    return log_a;

  if (log_a < log_b)
    return log_sum(log_b, log_a);

  // Now log_a >= log_b:
  return log_a + log1p(exp(log_b - log_a));
}

#endif
