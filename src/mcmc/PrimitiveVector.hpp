#ifndef _PRIMITIVEVECTOR_HPP
#define _PRIMITIVEVECTOR_HPP

#include <cstring>
#include <assert.h>
// for debugging:
//template <typename T>
//using PrimitiveVector = std::vector<T>;

template <typename T>
class PrimitiveVector {
private:
  static constexpr unsigned int START_SIZE = 4;

  unsigned long _size;
  unsigned long _capacity;
  T*__restrict _data;
public:

  PrimitiveVector():
    _size(0),
    _capacity(START_SIZE),
    _data( (T*) malloc(_capacity*sizeof(T)) )
  {
    assert(_data != NULL);
  }
  
  PrimitiveVector(unsigned long sz):
    _size(sz),
    _capacity(sz),
    _data( (T*) malloc(_capacity*sizeof(T)) )
  {
    assert(_data != NULL);
  }

  PrimitiveVector(unsigned long sz, const T & fill):
    _size(sz),
    _capacity(sz),
    _data( (T*) malloc(_capacity*sizeof(T)) )
  {
    assert(_data != NULL);
    for (unsigned long i=0; i<_size; ++i)
      _data[i] = fill;
  }

  PrimitiveVector(const PrimitiveVector & rhs):
    _size(rhs._size),
    _capacity(rhs._capacity),
    _data( (T*) malloc(_capacity*sizeof(T)) )
  {
    assert(_data != NULL);
    for (unsigned long i=0; i<_size; ++i)
      _data[i] = rhs._data[i];
  }
  
  ~PrimitiveVector() {
    free( _data );
  }

  const PrimitiveVector<T> & operator =(const PrimitiveVector & rhs) {
    _size = rhs._size;
    _capacity = rhs._capacity;

    _data = (T*)realloc(_data, _capacity*sizeof(T));
    assert(_data != NULL);
    for (unsigned long i=0; i<_size; ++i)
      _data[i] = rhs._data[i];

    return *this;
  }

  void push_back(const T & element) {
    if (_capacity == _size) {
      // Need to resize:
      _capacity += (_capacity>>1) + 1;
      _data = (T*)realloc(_data, _capacity*sizeof(T));

      assert(_data != NULL);
    }
    
    _data[_size] = element;
    ++_size;
  }

  void resize(const int new_size){
    // If it needs to expand, expand
    // else do nothing
    _size = new_size;
    if (_capacity < new_size){
      _capacity = new_size + 1;
      _data = (T*)realloc(_data, _capacity*sizeof(T));
      assert(_data != NULL);
    }
  }
  
  T pop(){
    --_size;
    return _data[_size];
  }
  
  void remove_item_at(unsigned long i){
    //NOTE: Should it throw an exception if i > (_size-1)?
    if (i < _size){
      if(i==_size-1){
	--_size;
      }else{
	memcpy(&_data[i], &_data[i+1], ((_size-i-1)*sizeof(T)));
	--_size;
      }
    }
  }

  const T & operator [](unsigned long i) const {
    return _data[i];
  }

  T & operator [](unsigned long i) {
    return _data[i];
  }

  void clear() {
    _size = 0;
    _capacity = START_SIZE;
    _data = (T*)realloc(_data, _capacity*sizeof(T));
  }

  unsigned long size() const {
    return _size;
  }
};

#endif
