#ifndef _SPECTRUM_HPP
#define _SPECTRUM_HPP

#include "PrimitiveVector.hpp"

#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <assert.h>
#include <iostream>
#include <math.h>
#include <string.h>

class Spectrum {
private:
  int _size;
  double* _sorted_mz;
  double* _log_intensity;
  double* _intensity;
  double _table_epsilon;
  double _one_over_table_epsilon;
  double _min_bin_mass;
  int _cumulative_table_size;
  int*_cumulative_count_table;
  std::vector<double> _binned_intensity;
  int _number_bins;
  constexpr static double _bin_epsilon=0.05;

  int mz_to_table_index(double mz) const {
    // This is a speedup over the following; neighboring bins are also
    // searched, so there should be no risk of numerical problems:
    //return int(mz / _table_epsilon);
    return int(mz * _one_over_table_epsilon);
  }

  // Find first index with _sorted_mz[index] > mz_goal
  __attribute__((always_inline))
  int find_first_index_geq_to_mz_log_search(double mz_goal, int lower, int upper) const {
    // Note: this while loop could be unrolled into a for loop using
    // the knowledge that it will take roughly log2(upper-lower+1)
    // steps
    int mid;
    while (lower < upper-1) {
      mid = (lower + upper)>>1;
      if (mz_goal < _sorted_mz[mid])
	// goal is in [lower, mid]
	upper = mid;
      else
	// goal is in [mid, upper]
	lower = mid;
    }
    
    if (mz_goal <= _sorted_mz[lower])
      return lower;
    if (mz_goal >= _sorted_mz[upper])
      // There exists no index with _sorted_mz[index] >= mz_goal
      return upper+1;
    return upper;
  }

  int find_first_index_geq_to_mz_naive(double mz_goal, int start_index, int end_index) const {
    // Note: result may be out of bounds if no m/z values have an
    // index such that _sorted_mz[index] >= mz_goal.
    int i;
    for (i=start_index; i<_size && i<=end_index; ++i)
      // Note: could be performed faster via SIMD?
      if (_sorted_mz[i] >= mz_goal)
	break;
    return i;
  }

  void bin_and_build_cumulative_distribution(){
    // bin:
    _cumulative_table_size = mz_to_table_index(_sorted_mz[_size-1])+1;
    _cumulative_count_table = new int[_cumulative_table_size];
    for (int i=0; i<_cumulative_table_size; ++i)
      _cumulative_count_table[i] = 0;

    for (int i=0; i<_size; ++i) {
      int bin_index = mz_to_table_index(_sorted_mz[i]);
      ++_cumulative_count_table[bin_index];
    }

    // accumulate:
    for (int i=0, cum_sum=0; i<_cumulative_table_size; ++i) {
      cum_sum += _cumulative_count_table[i];
      _cumulative_count_table[i] = cum_sum;
    }
  }


  void initiate_binning_data(){
    _min_bin_mass = _sorted_mz[0] - _bin_epsilon/2.0;
    _number_bins = ceil((_sorted_mz[_size - 1] - _sorted_mz[0]) / _bin_epsilon) + 1;
    _binned_intensity.resize(_number_bins);
    for (int i=0; i < _number_bins; ++i)
      _binned_intensity[i] = 0;
  }

  void bin_sorted_mz_with_max_intensity(const double max_intensity){
    initiate_binning_data();
    int peak_index = 0;
    for (int i=0; i < _number_bins; ++i){
      while (peak_index < _size && (_min_bin_mass + _bin_epsilon * i) <= _sorted_mz[peak_index] && _sorted_mz[peak_index] < (_min_bin_mass + _bin_epsilon * (i+1))){
	if (_binned_intensity[i] != 0)
	  _binned_intensity[i] = std::max(_intensity[peak_index], _binned_intensity[i]);
	
	else if (_intensity[peak_index]  < max_intensity)
	  _binned_intensity[i] = _intensity[peak_index];
	++peak_index;
      }
    }
  }

  void bin_sorted_mz(){
    initiate_binning_data();
    int peak_index = 0;
    for (int i=0; i < _number_bins; ++i){
      while (peak_index < _size && (_min_bin_mass + _bin_epsilon * i) <= _sorted_mz[peak_index] && _sorted_mz[peak_index] < (_min_bin_mass + _bin_epsilon * (i+1)) ){
	if (_binned_intensity[i] != 0)
	  _binned_intensity[i] = std::max(_intensity[peak_index], _binned_intensity[i]);
	
	else
	  _binned_intensity[peak_index] = _intensity[peak_index];
	
	++peak_index;
      }
    }
  }

  Spectrum(const PrimitiveVector<double> & sorted_mz, const PrimitiveVector<double> & intensities, double table_epsilon):
    _size(sorted_mz.size()),
    _table_epsilon(table_epsilon),
    _one_over_table_epsilon(1.0/table_epsilon)
  {
    assert(sorted_mz.size() == intensities.size());
    double min_intensity = 1.0/0.0;
    for (int i=0; i < intensities.size(); ++i)
      min_intensity = std::min(min_intensity, intensities[i]);
    
    _sorted_mz = new double[3*_size];
    _log_intensity = _sorted_mz + _size;
    _intensity = _sorted_mz + 2 * _size;
    for (int i=0; i<_size; ++i) {
      _sorted_mz[i] = sorted_mz[i];
      _intensity[i] = intensities[i];
      _log_intensity[i] = log1p(intensities[i] / min_intensity);
    }
    bin_and_build_cumulative_distribution();
    bin_sorted_mz();
  }
  
public:
  Spectrum(std::vector<std::pair<double, double> > & spectrum, double table_epsilon, double relative_intensity_cutoff):
    _table_epsilon(table_epsilon),
    _one_over_table_epsilon(1.0/table_epsilon)
  {
    double max_intensity = 0.0;
    for (auto x : spectrum)
      max_intensity = std::max(max_intensity, x.second);
    std::sort(spectrum.begin(), spectrum.end());
    _size = 0;

    double min_intensity = 1.0/0.0;
    for (auto x : spectrum)
      if (x.second/max_intensity >= relative_intensity_cutoff){
	min_intensity = std::min(min_intensity, x.second);
	++_size;
      }
    
    _sorted_mz = new double[3*_size];
    _log_intensity = _sorted_mz + _size;
    _intensity = _sorted_mz + 2 * _size;
    int index = 0;


        	
    for (unsigned int i=0; i<spectrum.size(); ++i) {
      if (spectrum[i].second/max_intensity >= relative_intensity_cutoff){
	_sorted_mz[index] = spectrum[i].first;
	_log_intensity[index] = log1p(spectrum[i].second/min_intensity);
	_intensity[index] = spectrum[i].second;
	++index;
      }
    }
    bin_and_build_cumulative_distribution();
    bin_sorted_mz_with_max_intensity(max_intensity);
  }

  Spectrum(const Spectrum & rhs) {
    _table_epsilon = rhs._table_epsilon;
    _one_over_table_epsilon = rhs._one_over_table_epsilon;
    _size = rhs._size;
    _sorted_mz = new double[3*_size];
    _log_intensity = _sorted_mz + _size;
    _intensity =  _sorted_mz + 2 * _size;
    _number_bins = rhs._number_bins;
    _binned_intensity.resize(_number_bins); // = _binned_sorted_mz + _number_bins;

    for (int i=0; i<_size; ++i) {
      _sorted_mz[i] = rhs._sorted_mz[i];
      _log_intensity[i] = rhs._log_intensity[i];
      _intensity[i] = rhs._intensity[i];
    }

    for (int i=0; i<_number_bins; ++i)
      _binned_intensity[i] = rhs._binned_intensity[i];

    _cumulative_table_size = rhs._cumulative_table_size;
    _cumulative_count_table = new int[_cumulative_table_size];

    for (int i=0; i<_cumulative_table_size; ++i)
      _cumulative_count_table[i] = rhs._cumulative_count_table[i];
  }

  Spectrum(Spectrum && rhs) {
    _size = 0;
    _sorted_mz = NULL;
    _log_intensity = NULL;
    _intensity = NULL;
    _number_bins = 0;
    _binned_intensity.empty();
    std::swap(_size, rhs._size);
    std::swap(_sorted_mz, rhs._sorted_mz);
    std::swap(_log_intensity, rhs._log_intensity);
    std::swap(_intensity, rhs._intensity);
    std::swap(_binned_intensity, rhs._binned_intensity);
    std::swap(_number_bins, rhs._number_bins);
    _cumulative_table_size = 0;
    _cumulative_count_table = NULL;
    std::swap(_cumulative_table_size, rhs._cumulative_table_size);
    std::swap(_cumulative_count_table, rhs._cumulative_count_table);

    _table_epsilon = rhs._table_epsilon;
    _one_over_table_epsilon = rhs._one_over_table_epsilon;
  }

  const Spectrum & operator =(const Spectrum & rhs) = delete;
  const Spectrum & operator =(Spectrum && rhs) = delete;
  
  ~Spectrum() {
    _size = 0;
    _cumulative_table_size = 0;
    _number_bins = 0;
    delete[] _sorted_mz;
    delete[] _cumulative_count_table;
  }

  void find_peaks_within_epsilon_linear_search(double mz_goal, double epsilon, int & start_index, int & window_size)  const {
    start_index = find_first_index_geq_to_mz_naive(mz_goal-epsilon, 0, _size-1);
    // searching for first peak > mz_goal + epsilon will not be in range; therefore, subtract 1:
    int end_index = find_first_index_geq_to_mz_naive(mz_goal+epsilon, 0, _size-1)-1;

    window_size = end_index - start_index + 1;
  }

  void find_peaks_within_epsilon_log_search(double mz_goal, double epsilon, int & start_index, int & window_size) const {
    start_index = find_first_index_geq_to_mz_log_search(mz_goal-epsilon, 0, _size-1);
    // searching for first peak > mz_goal + epsilon will not be in range; therefore, subtract 1:
    int end_index = find_first_index_geq_to_mz_log_search(mz_goal+epsilon, 0, _size-1)-1;

    window_size = end_index - start_index + 1;
  }

  void find_peaks_within_epsilon_binned(double mz_goal, double epsilon, int & start_index, int & window_size) const {
    // Epsilon for search must be <= _table_epsilon used to build the
    // cumulative table.  This guarantees that the window from the
    // cumulative table will be wider than needed for the epsilon
    // search.
    assert(epsilon <= _table_epsilon);

    int bin_index = std::min(std::max(mz_to_table_index(mz_goal), 0), _cumulative_table_size-1);

    // Want to use bin_index-1 in case of peaks near edges within
    // epsilon; however, _cumulative_count_table[bin_index-1]
    // includes those peaks in its total count. To exclude them, use
    // _cumulative_count_table[bin_index-2]. If no such bin
    // exists in the table, start the log search at 0:
    if (bin_index-2 > 0)
      start_index = std::max(_cumulative_count_table[bin_index-2], 0);
    else
      start_index = 0;
    int end_index = std::max(_cumulative_count_table[std::min(bin_index+1, _cumulative_table_size-1)]-1, 0);

    // Finish with log search
    start_index = find_first_index_geq_to_mz_log_search(mz_goal-epsilon, start_index, end_index);
    // searching for first peak > mz_goal + epsilon will not be in range; therefore, subtract 1:
    end_index = find_first_index_geq_to_mz_log_search(mz_goal+epsilon, start_index, end_index)-1;

    window_size = end_index - start_index + 1;
  }

  int find_most_intense_peak_within_epsilon_binned(double mz_goal, double epsilon) const {
    int start_index, window_size;
    find_peaks_within_epsilon_binned(mz_goal, epsilon, start_index, window_size);
    if (window_size <= 0)
      return -1;
    
    int max_intensity_index = start_index;
    for (int i=start_index+1; i<start_index+window_size; ++i)
      if (get_log_intensity(i) > get_log_intensity(max_intensity_index))
	max_intensity_index = i;
    return max_intensity_index;
  }
  
  double highest_log_intensity_near(double mz_goal, double epsilon) const {
    int index = find_most_intense_peak_within_epsilon_binned(mz_goal, epsilon);
    if (index != -1)
      return get_log_intensity(index);
    return log(0.0);
  }

  double highest_intensity_near(double mz_goal, double epsilon) const {
    int index = find_most_intense_peak_within_epsilon_binned(mz_goal, epsilon);
    if (index != -1)
      return get_intensity(index);
    return 0.0;
  }

  
  int number_peaks() const {
    return _size;
  }

  double get_mz(int index) const {
    return _sorted_mz[index];
  }

  double get_log_intensity(int index) const {
    return _log_intensity[index];
  }

  double get_intensity(int index) const {
    return _intensity[index];
  }

  Spectrum sample_at_mz(const PrimitiveVector<double> & sorted_mz, double epsilon) const {
    PrimitiveVector<double> intensities(sorted_mz.size(), 0.0);
    for (unsigned int i=0; i<sorted_mz.size(); ++i)
      intensities[i] = highest_intensity_near(sorted_mz[i], epsilon);
    return Spectrum(sorted_mz, intensities, _table_epsilon);
  }
  
  void scale_mz(double scaling_factor) {
    for (int i=0; i < _size; ++i)
      _sorted_mz[i] *= scaling_factor;
    delete[] _cumulative_count_table;
    bin_and_build_cumulative_distribution();
  }

  std::vector<double> get_binned_intensity(){
    return _binned_intensity;
  }
  
};

std::vector<Spectrum> load_mgf(char*fname, double epsilon, double table_epsilon, double relative_intensity_cutoff) {
  std::ifstream myfile(fname, std::ifstream::binary);
  std::string line;
  std::cout.precision(10);
  std::vector<Spectrum> spectra;
  if (myfile.is_open()){
    int ind = -1;
    while(!myfile.eof()){
      ind = ind + 1;
      getline(myfile,line);
      std::vector<std::pair<double, double> > spectrum;
      while(isdigit(line[0])){
	std::stringstream ss;
	ss << line;
	double mass, i;
	ss >> mass >> i;
	spectrum.push_back(std::make_pair(mass,i));
	getline(myfile,line);
	if(!isdigit(line[0]) && spectrum.size()>0)
	  spectra.push_back(Spectrum(spectrum, table_epsilon, relative_intensity_cutoff));
      }
    }
    myfile.close();
  }
  return spectra;
}

#endif
