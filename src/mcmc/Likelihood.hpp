#ifndef _LIKELIHOOD_HPP
#define _LIKELIHOOD_HPP

#include "log_sum.hpp"
#include "Graph.hpp"

#include <unordered_map>

const bool print_subgraph_peaks_as_mgf_llh = false;

class Likelihood{
protected:
  virtual double get_log_likelihood_of_charge_state_graphs_on_single_spectrum(const Spectrum *spect, const double* alphabet, int*buffer, int alphabet_size, const double epsilon, const int max_charge, const std::unordered_map<int, std::vector<PrimitiveVector<Edge> > > & charge_to_connected_graphs) = 0;

  virtual double get_log_likelihood_for_single_spectrum_and_multiple_charges(const Spectrum *spect, const double* alphabet, int* buffer, const int alphabet_size, const double epsilon, const int max_charge) {
    std::unordered_map<int, std::vector<PrimitiveVector<Edge> > > charge_to_connected_graphs;
    for (int charge=1; charge <= max_charge; ++charge) {
      Graph g = Graph(spect, alphabet, alphabet_size, epsilon, charge);
      charge_to_connected_graphs[charge] = g.get_connected_subgraphs(buffer, charge);
    }

    return get_log_likelihood_of_charge_state_graphs_on_single_spectrum(spect, alphabet, buffer, alphabet_size, epsilon, max_charge, charge_to_connected_graphs);
  }
  
public:

  virtual ~Likelihood(){}

  virtual double get_log_likelihood_for_all_spectra(const double* alphabet, const std::vector<Spectrum>* spectra, int* buffer, const int alphabet_size, const double epsilon, const int max_charge) {
    //double log_likelihood = log(0.0);
    double log_likelihood = 0.0;
    for (unsigned int i=0; i < spectra->size(); ++i){
      /* Next line used if printing out peaks connected by subgraphs */
      if (print_subgraph_peaks_as_mgf_llh)
	std::cout << "BEGIN IONS" << std::endl;
      
      //log_likelihood = log_sum(log_likelihood, get_log_likelihood_for_single_spectrum_and_multiple_charges(&(*spectra)[i], alphabet, buffer, alphabet_size, epsilon, max_charge));
      log_likelihood += get_log_likelihood_for_single_spectrum_and_multiple_charges(&(*spectra)[i], alphabet, buffer, alphabet_size, epsilon, max_charge);
      /* Next line used if printing out peaks connected by subgraphs */
      if (print_subgraph_peaks_as_mgf_llh)
	std::cout << "END IONS" << std::endl;
    }

    return log_likelihood;
  }
};

#endif
