#ifndef _PAIRHASH_HPP
#define _PAIRHASH_HPP

template <typename A, typename B>
struct PairHash {
  std::size_t operator() (const std::pair<A,B> & p) const {
    std::hash<A> hash_a;
    std::hash<B> hash_b;

    std::size_t combined = 0;
    combined ^= (hash_a(p.first)<<16)^89869747ul*3644798167ul;
    combined ^= (hash_b(p.second)<<16)^89869747ul*3644798167ul;

    return combined * 69069 + 907133923;
  }
};

#endif
