#ifndef _GRAPHISOMORPHISMLIKELIHOOD_HPP
#define _GRAPHISOMORPHISMLIKELIHOOD_HPP

#include "Likelihood.hpp"
#include "./Tensor/Tensor.hpp"
#include "./Convolution/fft_convolve.hpp"
#include "PairHash.hpp"
#include <unordered_set>

class GraphIsomorphismLikelihood : public Likelihood {
protected:
  virtual double get_log_likelihood_of_charge_state_graphs_on_single_spectrum(const Spectrum *spect, const double* alphabet, int* buffer, const int alphabet_size, const double epsilon, const int max_charge, const std::unordered_map<int, std::vector<PrimitiveVector<Edge> > > & charge_to_connected_graphs) override {
    std::unordered_set<std::pair<int, int>, PairHash<int,int> > edges_visited;
    std::array<std::pair<int,int>, 2 > best_subgraph_index_and_charges;
    double best_subgraph_pair_llh = -1;
    double log_likelihood = log(0.0);
    unsigned num_graphs = 0;
    for (const auto & p : charge_to_connected_graphs)
      num_graphs += p.second.size();
    if (num_graphs == 0)
      return 0.0;
    
    for (const auto & p : charge_to_connected_graphs) {
      int charge = p.first;
      const std::vector<PrimitiveVector<Edge> > & connected_subgraphs = p.second;
      
      for (unsigned int i=0; i<connected_subgraphs.size(); ++i) {
	const PrimitiveVector<Edge> & subgraph = connected_subgraphs[i];
	
	for (const auto & p2 : charge_to_connected_graphs) {
	  int charge2 = p2.first;
	  const std::vector<PrimitiveVector<Edge> > & connected_subgraphs2 = p2.second;

	  for (unsigned int i2=0; i2<connected_subgraphs2.size(); ++i2) {
	    const PrimitiveVector<Edge> & subgraph2 = connected_subgraphs2[i2];

	    if (charge <= charge2 || i <= i2){
	      double graph_similarity_log_likelihood = graph_similarity(spect, subgraph, charge, subgraph2, charge2, epsilon, i==i2);
	      log_likelihood = log_sum(log_likelihood, graph_similarity_log_likelihood);
	      if (print_subgraph_peaks_as_mgf_llh)
		if (graph_similarity_log_likelihood > best_subgraph_pair_llh){
		  best_subgraph_pair_llh = graph_similarity_log_likelihood;
		  best_subgraph_index_and_charges[0].first = i;  // subgraph index
		  best_subgraph_index_and_charges[0].second = charge; // charge
		  best_subgraph_index_and_charges[1].first = i2;
		  best_subgraph_index_and_charges[1].second = charge2;
		}
	    }
	  }
	}
      }
    }

    if (print_subgraph_peaks_as_mgf_llh)
      for (const auto & p : charge_to_connected_graphs) {
	if (p.first == best_subgraph_index_and_charges[0].second || p.first == best_subgraph_index_and_charges[1].second){
	  int subgraph_index;
	  if (p.first == best_subgraph_index_and_charges[0].second)
	    subgraph_index = best_subgraph_index_and_charges[0].first;

	  if (p.first == best_subgraph_index_and_charges[1].second)
	    subgraph_index = best_subgraph_index_and_charges[1].first;
	  
	  const std::vector<PrimitiveVector<Edge> > & connected_subgraphs = p.second;
	  const PrimitiveVector<Edge> & subgraph = connected_subgraphs[subgraph_index];
	  for (unsigned i=0; i < subgraph.size(); ++i){
	    std::cout << spect->get_mz(subgraph[i].start_peak_index) << " " << spect->get_log_intensity(subgraph[i].start_peak_index) << std::endl;
	    std::cout << spect->get_mz(subgraph[i].end_peak_index) << " " << spect->get_log_intensity(subgraph[i].end_peak_index) << std::endl;
	  }
	}
      }
    return log_likelihood;
  }
  
  double graph_similarity(const Spectrum*spect, const PrimitiveVector<Edge> & subgraph, int charge, const PrimitiveVector<Edge> & subgraph2, int charge2, double epsilon, bool forbid_zero_shift) {
    std::unordered_set<int> peaks, peaks2;
    for (unsigned int i=0; i<subgraph.size(); ++i) {
      const Edge & e = subgraph[i];
      peaks.insert(e.start_peak_index);
      peaks.insert(e.end_peak_index);
    }
    for (unsigned int i=0; i<subgraph2.size(); ++i) {
      const Edge & e = subgraph2[i];
      peaks2.insert(e.start_peak_index);
      peaks2.insert(e.end_peak_index);
    }

    PrimitiveVector<double> mz(peaks.size()), mz2(peaks2.size());
    int index = 0;
    for (int i : peaks) {
      mz[index] = spect->get_mz(i);
      ++index;
    }
    index = 0;
    for (int i : peaks2) {
      mz2[index] = spect->get_mz(i);
      ++index;
    }

    std::sort(&mz[0], &mz[0] + mz.size());
    std::sort(&mz2[0], &mz2[0] + mz2.size());

    return sparse_max_log_correlation(spect, mz, charge, mz2, charge2, epsilon, forbid_zero_shift);
  }

  double sparse_max_log_correlation(const Spectrum*spect, const PrimitiveVector<double> & mz, int charge, const PrimitiveVector<double> & mz2, int charge2, double epsilon, bool forbid_zero_shift) {
    double result = 0.0;
    Spectrum sub_lhs(spect->sample_at_mz(mz, epsilon));
    Spectrum sub_rhs(spect->sample_at_mz(mz2, epsilon));
    Tensor<double> lhs_tensor({1, sub_lhs.number_peaks()}, sub_lhs.get_binned_intensity());
    Tensor<double> rhs_tensor({1, sub_rhs.number_peaks()}, sub_rhs.get_binned_intensity());
    sub_lhs.scale_mz(charge);
    sub_rhs.scale_mz(charge2);
    Tensor<double> fftout = fft_convolve(lhs_tensor, rhs_tensor);
    for (unsigned long i = 0; i < fftout.flat_size(); ++i)
      result = std::max(result, fftout[i]);
    return log(result);
  }

public:
  GraphIsomorphismLikelihood(){
    std::cout << "Using llh model GraphIsomorphismLikelihood " << std::endl;
  }
};

#endif
