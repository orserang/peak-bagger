#ifndef _SAMPLE_FROM_ARRAY_HPP
#define _SAMPLE_FROM_ARRAY_HPP

int sample_from_array_sum_unknown(double*vals, int size) {
  int i=0;

  double tot = log(0.0);
  for (i=0; i<size; ++i)
    tot = log_sum(tot, vals[i]);

  double cum_sum = log(0.0);
  double goal = ((double) rand() / (RAND_MAX))*tot;
  for(i=0; i < size-1 && cum_sum < goal; ++i)
    cum_sum = log_sum(cum_sum, vals[i]);

  return i;
}

#endif
