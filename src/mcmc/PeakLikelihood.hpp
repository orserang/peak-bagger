#ifndef _PEAKLIKELIHOOD_HPP
#define _PEAKLIKELIHOOD_HPP

#include "ConditionallyIndependentChargeLikelihood.hpp"
#include <unordered_set>

class PeakLikelihood : public ConditionallyIndependentChargeLikelihood {
private:
  double get_log_likelihood_of_single_connected_graph(const Spectrum *spect, const PrimitiveVector<Edge> & subgraph) override {
    // todo: this is slow; could be implemented using an array
    std::unordered_set<int> peaks_visited;

    double llh = 0;
    for (unsigned int i=0; i < subgraph.size(); ++i){
      if (peaks_visited.find(subgraph[i].start_peak_index) == peaks_visited.end())
	llh += spect->get_log_intensity(subgraph[i].start_peak_index);
      if (peaks_visited.find(subgraph[i].end_peak_index) == peaks_visited.end())
	llh += spect->get_log_intensity(subgraph[i].end_peak_index);

      peaks_visited.insert(subgraph[i].start_peak_index);
      peaks_visited.insert(subgraph[i].end_peak_index);
    }
    return llh;
  }

public:
  PeakLikelihood(){
    std::cout << "Using llh model PeakLikelihood " << std::endl;
  }
};

#endif
