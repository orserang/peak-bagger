import sys

#  File to interpret the output from running the MCMC program with the
#  -r flag.

def main(args):
  if len(args) < 3:
    print 'usage: <top number to show> <epsilon> <file>'
    return

  k = int(args[0])
  epsilon = float(args[1])
  masses_per_thread = []
  for line in open(args[2], 'r'):
    ls = line.split()
    if ls[0] == "Thread":
      masses_per_thread.append([])
      continue

    masses_per_thread[-1].append( [float(ls[0]),int(ls[1])] )

  refined_list = []
  for i in range(len(masses_per_thread)):
    masses_per_thread[i] = sorted(masses_per_thread[i])
    new_mass_tuple_list = []
    j = 0
    while j < len(masses_per_thread[i]):
      mass_count = masses_per_thread[i][j]
      j += 1
      while (j < len(masses_per_thread[i])) and (abs(masses_per_thread[i][j][0] - mass_count[0]) < epsilon):
        mass_count[1] += masses_per_thread[i][j][1]
        j += 1

      new_mass_tuple_list.append(mass_count)
    refined_list.append( sorted(new_mass_tuple_list, reverse=True, key=lambda x:x[1])  )

  all_compiled = []
  for i in range(len(masses_per_thread)):
    masses_per_thread[i] = sorted(masses_per_thread[i])
    new_mass_tuple_list = []
    j = 0
    while j < len(masses_per_thread[i]):
      mass_count = masses_per_thread[i][j]
      j += 1
      while (j < len(masses_per_thread[i])) and (abs(masses_per_thread[i][j][0] - mass_count[0]) < epsilon):
        mass_count[1] += masses_per_thread[i][j][1]
        j += 1

      all_compiled.append(mass_count)
  all_compiled = sorted(all_compiled, reverse=True, key=lambda x:x[1])
  for i in range(len(refined_list)):
    print '\n\nThread ', i
    for j in range(min(k,len(refined_list[i]))):
      print refined_list[i][j][0], '\t',  refined_list[i][j][1]

  print '\n\nAll threads '
  for j in range(min(k,len(refined_list[i]))):
    print all_compiled[j][0], '\t',  all_compiled[j][1]

  for i in range(len(refined_list)):
    if k >= len(refined_list[i]):
      print 'Amount requested to print larger than possible'
      break

if __name__ == '__main__':
    main(sys.argv[1:])
