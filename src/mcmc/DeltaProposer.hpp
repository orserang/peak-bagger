#ifndef _DELTAPROPOSER_HPP
#define _DELTAPROPOSER_HPP

#include "PrimitiveVector.hpp"
#include "sample_from_array.hpp"

class DeltaProposer {
protected:
  const std::vector<Spectrum> & _spectra;
  const double _epsilon;
  const int _alphabet_size;
  const int _max_charge;
  const int _number_protected_seeds;
  PrimitiveVector<double> _cumulative_neutral_loss_probabilities;
  PrimitiveVector<double> _histogram_neutral_losses;
  PrimitiveVector<double> _prod_peak_intensities;

  // todo: refactor (possibly using sample_from_log_array_sum_unknown)
  void calculate_histogram(){
    std::vector<std::pair<double, double> > deltas_and_intensities;
    for (const Spectrum &s : _spectra)
      for (int i=0; i < s.number_peaks(); ++i)
	for (int j=i+1; j < s.number_peaks(); ++j)
	  deltas_and_intensities.push_back(std::make_pair(s.get_mz(j) - s.get_mz(i), s.get_log_intensity(j) * s.get_log_intensity(i)));
    std::sort(deltas_and_intensities.begin(), deltas_and_intensities.end());
    
    int number_deltas = deltas_and_intensities.size();
    double hist_width = fabs(deltas_and_intensities[0].first - deltas_and_intensities[deltas_and_intensities.size() - 1].first);
    double bin_width = _epsilon/2.0;
    long num_bins = 1 + (long)hist_width/bin_width;
    PrimitiveVector<double> bins(num_bins);
    PrimitiveVector<double> frequency(num_bins);    
    for (int i=0; i<num_bins; ++i){
      bins[i] = 0;
      frequency[i] = 0;
    }
    int bin_counter = 0;
    int delta_counter = 0;
    double bin_value = deltas_and_intensities[0].first;
    while(bin_counter < num_bins && delta_counter < number_deltas){
      bins[bin_counter] = bin_value;
      while(delta_counter < number_deltas && bin_value <= deltas_and_intensities[delta_counter].first && deltas_and_intensities[delta_counter].first < bin_value + _epsilon/2.0){
	frequency[bin_counter] += deltas_and_intensities[delta_counter].second;
	++delta_counter;
      }
      bin_value += bin_width;
      ++bin_counter;
    }

    double cum_sum = 0.0;
    for (unsigned int i=0; i<bins.size(); ++i)
      if (frequency[i] > 0){
	cum_sum += frequency[i];
	_prod_peak_intensities.push_back(frequency[i]);
	_histogram_neutral_losses.push_back(bins[i]);
	_cumulative_neutral_loss_probabilities.push_back(cum_sum);
      }

    for (unsigned int i=0; i<_cumulative_neutral_loss_probabilities.size(); ++i)
      _cumulative_neutral_loss_probabilities[i] = _cumulative_neutral_loss_probabilities[i] / cum_sum;
  }


  double random_histogram_delta() const {
    // Note: Currently implemented in O(n) worst-case, but can be done
    // in sublinear time (log search or table).

    // sample from histogram:
    double random_variable = ((double) rand() / (RAND_MAX));
    unsigned int i=0;
    while(i < _cumulative_neutral_loss_probabilities.size()-1 && _cumulative_neutral_loss_probabilities[i] < random_variable)
      ++i;

    return _histogram_neutral_losses[i];
  }

  double random_charge_changed_delta(const double*alphabet, int delta_index) const {
    int charge_val = rand()%_max_charge+1;
    if (rand()%2==0)
      return alphabet[delta_index]*charge_val;
    else
      return alphabet[delta_index]/charge_val;
  }

  double random_graph_connected_delta(const double*alphabet, int delta_index, int*buffer) const {
    double candidate_delta;

    int spectrum_index = rand() % _spectra.size();
    int charge = rand()%_max_charge+1;

    const Spectrum & spect = _spectra[spectrum_index];
    // Note: slow. rebuilds graph from scratch every time.
    Graph g(&spect, alphabet, _alphabet_size, _epsilon, charge);
    std::vector<PrimitiveVector<Edge> > connected_subgraphs = g.get_connected_subgraphs(buffer, charge);

    PrimitiveVector<int> peak_indices;
    for (const PrimitiveVector<Edge> & subgraph_edges : connected_subgraphs) {
      for (unsigned int i=0; i<subgraph_edges.size(); ++i) {
	const Edge & e = subgraph_edges[i];
	peak_indices.push_back(e.start_peak_index);
	peak_indices.push_back(e.end_peak_index);
      }
    }

    if (peak_indices.size() > 0) {
      int peak_index = peak_indices[rand()%peak_indices.size()];

      // Sample from other peaks weighted by intensity:
      double*other_peak_log_quality = (double*)buffer;
      for (int i=0; i<spect.number_peaks(); ++i)
	other_peak_log_quality[i] = spect.get_log_intensity(i);
      int other_peak_index = sample_from_array_sum_unknown(other_peak_log_quality, spect.number_peaks());

      candidate_delta = fabs(spect.get_mz(peak_index) - spect.get_mz(other_peak_index))*charge;
    }
    // No graph exists yet:
    else
      candidate_delta = fabs(spect.get_mz(rand()%spect.number_peaks()) - spect.get_mz(rand()%spect.number_peaks()));

    return candidate_delta;
  }


  bool change_charge_on_random_subgraph(const Spectrum & spect, double*alphabet, int*buffer, int old_charge, double charge_scaling_factor) const {
    // Build graph:
    Graph g(&spect, alphabet, _alphabet_size, _epsilon, old_charge);
    std::vector<PrimitiveVector<Edge> > connected_subgraphs = g.get_connected_subgraphs(buffer, old_charge);

    if (connected_subgraphs.size() == 0)
      // No graphs to sample from; failure
      return false;

    PrimitiveVector<int> peak_to_subgraph_number(spect.number_peaks(), -1);
    int subgraph_index=0;
    for (const PrimitiveVector<Edge> & subgraph_edges : connected_subgraphs) {
      for (unsigned int i=0; i<subgraph_edges.size(); ++i) {
	const Edge & e = subgraph_edges[i];
	peak_to_subgraph_number[e.start_peak_index] = subgraph_index;
	peak_to_subgraph_number[e.end_peak_index] = subgraph_index;
      }
      
      ++subgraph_index;
    }

    int random_peak = rand() % spect.number_peaks();
    subgraph_index = peak_to_subgraph_number[random_peak];

    while (subgraph_index == -1) {
      random_peak = (random_peak + 1) % spect.number_peaks();
      subgraph_index = peak_to_subgraph_number[random_peak];
    }
    
    const PrimitiveVector<Edge> & subgraph_edges = connected_subgraphs[subgraph_index];
    std::unordered_set<int> alphabet_indices_used;
    for (unsigned int i=0; i<subgraph_edges.size(); ++i) {
      const Edge & e = subgraph_edges[i];
      alphabet_indices_used.insert(e.delta_index);
    }
    
    PrimitiveVector<std::pair<int, double> > result;

    for (int i : alphabet_indices_used) {
      double new_delta = (alphabet[i]*old_charge) / charge_scaling_factor;
      alphabet[i] = new_delta;
    }

    // Note: this alphabet change is not verified; can do this later if necessary.
    
    // Success
    return true;
  }

  
  
public:
  DeltaProposer(const std::vector<Spectrum> &spectra, const double epsilon, const int alphabet_size, const int max_charge, const int number_protected_seeds):
    _spectra(spectra),
    _epsilon(epsilon),
    _alphabet_size(alphabet_size),
    _max_charge(max_charge),
    _number_protected_seeds(number_protected_seeds)
  {
    calculate_histogram();
  }

  
  void propose_block_wise_change(double*alphabet, int*buffer) const {
    const Spectrum & spect = _spectra[rand()%_spectra.size()];
    int old_charge = rand()%_max_charge+1;
    double charge_scaling_factor = rand()%_max_charge+1; 
    if (rand()%2 == 0)
      charge_scaling_factor = 1.0 / charge_scaling_factor;
    change_charge_on_random_subgraph(spect, alphabet, buffer, old_charge, charge_scaling_factor);
  }

  
  std::pair<int,double> propose_delta(const double* alphabet, int*buffer, int alphabet_size_to_consider_for_conflicts) const {
    for (;;) {
      int delta_index = rand() % (_alphabet_size - _number_protected_seeds) + _number_protected_seeds;
      double candidate_delta;

      int choice = rand()%3;
      if (choice==0)
	candidate_delta = random_histogram_delta();
      else if (choice==1)
	candidate_delta = random_charge_changed_delta(alphabet, delta_index);
      else
	candidate_delta = random_graph_connected_delta(alphabet, delta_index, buffer);

      if (alphabet_change_is_valid(alphabet, delta_index, candidate_delta, alphabet_size_to_consider_for_conflicts))
	return std::make_pair(delta_index, candidate_delta);
    }
  }

  
  std::pair<int,double> propose_initial_delta(const double* alphabet, int*buffer, int alphabet_size_to_consider_for_conflicts) const {
    for (;;) {
      int delta_index = rand() % (_alphabet_size - _number_protected_seeds) + _number_protected_seeds;
      double candidate_delta;

      int choice = rand()%2;
      if (choice==0)
	candidate_delta = random_histogram_delta();
      else
	candidate_delta = random_graph_connected_delta(alphabet, delta_index, buffer);

      if (alphabet_change_is_valid(alphabet, delta_index, candidate_delta, alphabet_size_to_consider_for_conflicts))
	return std::make_pair(delta_index, candidate_delta);
    }
  }

  virtual bool alphabet_change_is_valid(const double* alphabet, int & delta_index, double & candidate_delta, int alphabet_size_to_consider_for_conflicts) const {
    if (candidate_delta < 1 - _epsilon)
      return false;

    // the following line disables more advanced redundancy checking
    // (this may be appropriate if the model does not reward redundancy)
    //return true;

    int num_matches = 0;
    // Check if new delta is charge-multiple of existing delta
    for (int k=0; k<alphabet_size_to_consider_for_conflicts; ++k)
      for (int charge=1; charge<=_max_charge; ++charge)
	for (int charge2=1; charge2<=_max_charge; ++charge2) {
	  double mz1 = candidate_delta/charge;
	  double mz2 = alphabet[k]/charge2;
	  
	  if (fabs( 1 - std::max(mz1,mz2)/std::min(mz1,mz2)) < _epsilon || fabs(mz1-mz2)<_epsilon) {
	    if (k >= _number_protected_seeds) {
	      if (num_matches>=1)
		return false;

	      delta_index = k;
	      ++num_matches;
	    }
	    else
	      return false;
	  }
	}

    return true;
  }

  virtual ~DeltaProposer() {}
  
};

#endif
