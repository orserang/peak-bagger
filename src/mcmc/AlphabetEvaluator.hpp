#ifndef _ALPHABETEVALUATOR_HPP
#define _ALPHABETEVALUATOR_HPP

#include <iomanip>
#include <fstream>
#include <set>


class AlphabetEvaluator{
private:
  const std::vector<Spectrum> _spectra;
  const double _epsilon;
  const int _max_charge;
  Likelihood * _model;
  const std::vector<std::pair<std::string, double> > _whitelist;

  void create_annotations_for_whitelist(const double* alphabet, std::vector<std::string>& annotations, std::vector<std::string>& charge_strings, const int alphabet_size){
    for (int i=0; i<alphabet_size; ++i){
      annotations.push_back("");
      charge_strings.push_back("");
    }
    
    std::setprecision(2);
    
    std::vector<double> charges;
    for (double charge=1; charge <= _max_charge; ++charge)
      charges.push_back(charge);
    for (double charge=2; charge <= _max_charge; ++charge)
      charges.push_back(1.0/charge);

    if (!_whitelist.empty()){
      for (int i=0; i<alphabet_size; ++i){
	for (unsigned int j=0; j<_whitelist.size(); ++j){
	  for (auto charge : charges){
	    if (fabs(alphabet[i] - _whitelist[j].second/charge) < _epsilon){
	      annotations[i] += _whitelist[j].first + ", ";
	      charge_strings[i] += std::to_string(charge).substr(0,3) + ", ";
	    }
	  }
	}
      }
    }
  }

  void print_largest_subgraph_spectrum(const Spectrum & s, const std::vector<PrimitiveVector<Edge> > &connected_graphs, std::ofstream &write_out_file){
    if (connected_graphs.size() > 0){
      unsigned largest_connected_subgraph_index = 0;
      unsigned largest_connected_subgraph_size = 0;
      for (unsigned i = 0; i < connected_graphs.size(); ++i){
        if (connected_graphs[i].size() > largest_connected_subgraph_size){
      	largest_connected_subgraph_size = connected_graphs[i].size();
      	largest_connected_subgraph_index = i;
        }	
      }

      std::set<int> peaks_in_largest_subgraph;
      const PrimitiveVector<Edge> & largest_cg = connected_graphs[largest_connected_subgraph_index];
      for (unsigned i = 0; i < largest_cg.size(); ++i){
        peaks_in_largest_subgraph.insert(largest_cg[i].start_peak_index);
        peaks_in_largest_subgraph.insert(largest_cg[i].end_peak_index);
      }
      write_out_file << "BEGIN IONS" << std::endl;
      for (auto peak : peaks_in_largest_subgraph)
        write_out_file << s.get_mz(peak) << " " << s.get_log_intensity(peak) << std::endl;
      write_out_file << "END IONS" << std::endl;

    }
  }

  void change_to_most_connected_charge_states(double* alphabet, const int alphabet_size){
    int max_spectrum_size = 0;
    for (const Spectrum &s : _spectra)
      max_spectrum_size = std::max(max_spectrum_size, s.number_peaks());

    int extended_alphabet_size =  alphabet_size * _max_charge;
    double* alphabet_all_charges = (double*) malloc(alphabet_size * _max_charge * sizeof(double));

    int i = 0;
    for (int j = 0; j < extended_alphabet_size; ++j){
      for (int charge=1; charge<=_max_charge; ++charge){
	alphabet_all_charges[i] = alphabet[j]/double(charge);
	++i;
      }
    }

    std::sort(&alphabet_all_charges[0], &alphabet_all_charges[extended_alphabet_size - 1]);
    std::vector<int*> delta_count(_max_charge);

    for (int i=0; i < _max_charge; ++i){
      delta_count[i] = new int[extended_alphabet_size];
      for (int j=0; j<extended_alphabet_size; ++j){
	delta_count[i][j] = 0;         
      }
    }
    int* buffer = (int*) malloc(max_spectrum_size * sizeof(int) + max_spectrum_size * sizeof(int));
    double log_likelihood = _model->get_log_likelihood_for_all_spectra(alphabet_all_charges, &(_spectra), buffer, extended_alphabet_size, _epsilon, _max_charge);

    for (const Spectrum &s : _spectra){      
      for (int charge=1; charge<=_max_charge; ++charge) {
	Graph g(&s, alphabet_all_charges, extended_alphabet_size, _epsilon, charge);
	std::vector<PrimitiveVector<Edge> > connected_graphs = g.get_connected_subgraphs(buffer, charge);
	for (const auto &cg : connected_graphs){
	  
	  for (unsigned int i=0; i < cg.size(); ++i){
	    int delta = cg[i].delta_index;
	    ++delta_count[charge - 1][delta];
	  }
	}
      }
    }


    std::vector<int> total_counts_per_delta(extended_alphabet_size);
    for (int ind=0; ind < extended_alphabet_size; ++ind)
      total_counts_per_delta[ind] = 0;

    for (int charge=1; charge<=_max_charge; ++charge) {
      for (int i=0; i<extended_alphabet_size; ++i) {
	total_counts_per_delta[i] += delta_count[charge - 1][i];
      }

    }

    int i_reduced = 0;
    for (int i=0; i<extended_alphabet_size; i += _max_charge, ++i_reduced) {
      int best_index = 0;
      int best_count = 0;
      for (int j=0; j<_max_charge; ++j) {
	if (total_counts_per_delta[i + j] > best_count){
	  best_count = total_counts_per_delta[i + j];
	  best_index = j;
	}
      }
      alphabet[i_reduced] = alphabet_all_charges[i + best_index];
    }
    
    print_simple_annotated_alphabet(alphabet, alphabet_size);
    for (int i=0; i < _max_charge; ++i){
      delete[] delta_count[i];         
    }
    free(buffer);


  }
  
  
public:
  AlphabetEvaluator(const std::vector<Spectrum> & spectra, const std::vector<std::pair<std::string, double> > &whitelist, const double epsilon, const int max_charge, Likelihood*model):
    _spectra(spectra),
    _epsilon(epsilon),
    _max_charge(max_charge),
    _model(model),
    _whitelist(whitelist)
  {}
  void public_change_to_most_connected_charge_states(double* alphabet, const int alphabet_size){
    change_to_most_connected_charge_states(alphabet, alphabet_size);
  }
  void print_simple_annotated_alphabet(const double* alphabet, const int alphabet_size){
    std::cout << "\nBest Alphabet (only z=1):\n";
    for (int i = 0; i < alphabet_size; ++i){
      bool printed = false;
      for (unsigned int j = 0; j < _whitelist.size(); ++j){
	if (fabs(alphabet[i] - _whitelist[j].second) < _epsilon){
	  std::cout << std::to_string(alphabet[i]) << "\t" << _whitelist[j].first << std::endl;
	  printed = true;
	}
      }
      if (!printed)
	std::cout << alphabet[i] << std::endl;  
    }
  }
  
  void print_stats(double* alphabet, const int alphabet_size){
    /*
      int number of instances of delta in all spectra
      int number of spectra delta appears in
      int total size of graphs delat appears in
      int number of graphs delta appears in
      double total log intensity of peaks touched
    */
    std::sort(&alphabet[0], &alphabet[alphabet_size - 1]);

    std::vector<int*>    delta_count(_max_charge);
    std::vector<int*>    tot_size_graphs(_max_charge);
    std::vector<int*>    num_graphs_delta_in(_max_charge);
    std::vector<bool*>   in_graph(_max_charge);
    std::vector<double*> tot_lg_intensities(_max_charge);
    for (int i=0; i < _max_charge; ++i){
      delta_count[i]         = new int[alphabet_size];
      tot_size_graphs[i]     = new int[alphabet_size];
      num_graphs_delta_in[i] = new int[alphabet_size];
      in_graph[i]            = new bool[alphabet_size];
      tot_lg_intensities[i]  = new double[alphabet_size];

      for (int j=0; j<alphabet_size; ++j){
	in_graph[i][j] = 0;
	delta_count[i][j] = 0;         
	tot_size_graphs[i][j] = 0;
	tot_lg_intensities[i][j] = 0;           
	num_graphs_delta_in[i][j] = 0;          
      }
    }
    std::cout << "Initialized all\n";

    int max_spectrum_size = 0;
    for (const Spectrum &s : _spectra)
      max_spectrum_size = std::max(max_spectrum_size, s.number_peaks());

    int* buffer = (int*) malloc(max_spectrum_size * sizeof(int) + max_spectrum_size * sizeof(int));
    double log_likelihood = _model->get_log_likelihood_for_all_spectra(alphabet, &(_spectra), buffer, alphabet_size, _epsilon, _max_charge);

    std::ofstream write_out_file;
    write_out_file.open("subgraph_spectrum.mgf");
    
    for (const Spectrum &s : _spectra){      
      for (int charge=1; charge<=_max_charge; ++charge) {
	Graph g(&s, alphabet, alphabet_size, _epsilon, charge);
	std::vector<PrimitiveVector<Edge> > connected_graphs = g.get_connected_subgraphs(buffer, charge);
	//print_largest_subgraph_spectrum(s, connected_graphs, write_out_file);
	for (const auto &cg : connected_graphs){
	  for (int i=0; i<alphabet_size; ++i)
	    in_graph[charge - 1][i] = 0;
	  
	  for (unsigned int i=0; i < cg.size(); ++i){
	    int delta = cg[i].delta_index;
	    ++delta_count[charge - 1][delta];
	    in_graph[charge - 1][delta] = true;
	    tot_lg_intensities[charge - 1][delta] += s.get_log_intensity(cg[i].start_peak_index) + s.get_log_intensity(cg[i].end_peak_index);
	  }
	  for (int i=0; i<alphabet_size; ++i)
	    if (in_graph[charge - 1][i]){
	      ++num_graphs_delta_in[charge - 1][i];
	      tot_size_graphs[charge - 1][i] += cg.size();
	    }
	}
      }
    }

    write_out_file.close();
    std::vector<int> total_counts_per_delta(alphabet_size);
    for (int ind=0; ind < alphabet_size; ++ind)
      total_counts_per_delta[ind] = 0;

    for (int charge=1; charge<=_max_charge; ++charge) {
      std::vector<std::string> annotations;
      std::vector<std::string> charges;
      create_annotations_for_whitelist(alphabet, annotations, charges, alphabet_size);
      printf("\t\t<<<<<<<<<<Stats for charge %d>>>>>>>>>>\n", charge);
      std::cout << "Log likelihood: " << log_likelihood << std::endl;
      printf("%-15s%-10s%-10s%-15s%-15s%-20s%-10s\n", "delta", "count", "num grphs", "avg G size", "avg lg(i)", "whitelist", "charge");

      for (int i=0; i<alphabet_size; ++i) {
	total_counts_per_delta[i] += delta_count[charge - 1][i];
        printf("%-15f", alphabet[i]);
        printf("%-10i", delta_count[charge - 1][i]);
        printf("%-10i", num_graphs_delta_in[charge - 1][i]);
        printf("%-15f", double(tot_size_graphs[charge - 1][i])/num_graphs_delta_in[charge - 1][i]);
        printf("%-15f", double(tot_lg_intensities[charge - 1][i])/delta_count[charge - 1][i]);
        printf("%-20s", annotations[i].c_str());
        printf("%-10s", charges[i].c_str());
        printf("\n");
	
      }
      std::cout << std::endl;
    }
    printf("%-15s%-10s\n", "delta", "count over all charges");
    for (int i=0; i<alphabet_size; ++i) {
      printf("%-15f", alphabet[i]);
      printf("%-10i", total_counts_per_delta[i]);
      printf("\n");
    }
    std::cout << std::endl;


    print_simple_annotated_alphabet(alphabet, alphabet_size);
    for (int i=0; i < _max_charge; ++i){
      delete[] delta_count[i];         
      delete[] tot_size_graphs[i];     
      delete[] num_graphs_delta_in[i]; 
      delete[] in_graph[i];            
      delete[] tot_lg_intensities[i];
    }
    free(buffer);
    
  }


  void canon(double* alphabet, const int alphabet_size){
    int max_spectrum_size = 0;
    for (const Spectrum &s : _spectra)
      max_spectrum_size = std::max(max_spectrum_size, s.number_peaks());

    int extended_alphabet_size =  alphabet_size * _max_charge;
    double* alphabet_all_charges = (double*) malloc(alphabet_size * _max_charge * sizeof(double));

    int i = 0;
    for (int j = 0; j < extended_alphabet_size; ++j){
      for (int charge=1; charge<=_max_charge; ++charge){
	alphabet_all_charges[i] = alphabet[j]/double(charge);
	++i;
      }
    }

    std::sort(&alphabet_all_charges[0], &alphabet_all_charges[extended_alphabet_size - 1]);
    std::vector<int*> delta_count(_max_charge);

    for (int i=0; i < _max_charge; ++i){
      delta_count[i] = new int[extended_alphabet_size];
      for (int j=0; j<extended_alphabet_size; ++j){
	delta_count[i][j] = 0;         
      }
    }
    int* buffer = (int*) malloc(max_spectrum_size * sizeof(int) + max_spectrum_size * sizeof(int));
    double log_likelihood = _model->get_log_likelihood_for_all_spectra(alphabet_all_charges, &(_spectra), buffer, extended_alphabet_size, _epsilon, _max_charge);

    for (const Spectrum &s : _spectra){      
      for (int charge=1; charge<=_max_charge; ++charge) {
	Graph g(&s, alphabet_all_charges, extended_alphabet_size, _epsilon, charge);
	std::vector<PrimitiveVector<Edge> > connected_graphs = g.get_connected_subgraphs(buffer, charge);
	for (const auto &cg : connected_graphs){
	  
	  for (unsigned int i=0; i < cg.size(); ++i){
	    int delta = cg[i].delta_index;
	    ++delta_count[charge - 1][delta];
	  }
	}
      }
    }


    std::vector<int> total_counts_per_delta(extended_alphabet_size);
    for (int ind=0; ind < extended_alphabet_size; ++ind)
      total_counts_per_delta[ind] = 0;

    for (int charge=1; charge<=_max_charge; ++charge) {
      for (int i=0; i<extended_alphabet_size; ++i) {
	total_counts_per_delta[i] += delta_count[charge - 1][i];
      }

    }

    int i_reduced = 0;
    for (int i=0; i<extended_alphabet_size; i += _max_charge, ++i_reduced) {
      int best_index = 0;
      int best_count = 0;
      for (int j=0; j<_max_charge; ++j) {
	if (total_counts_per_delta[i + j] > best_count){
	  best_count = total_counts_per_delta[i + j];
	  best_index = j;
	}
      }
      alphabet[i_reduced] = alphabet_all_charges[i + best_index];
    }
    
    print_simple_annotated_alphabet(alphabet, alphabet_size);
    for (int i=0; i < _max_charge; ++i){
      delete[] delta_count[i];         
    }
    free(buffer);

  }


};
#endif
