#include <iostream>
#include <vector>
#include "ElementalDecomposer.hpp"


std::string clean_mass(std::string mass){
  std::string output;
  for (int i=0; i<mass.length(); ++i){
    if (isdigit(mass[i]) || mass[i]=='.')
      output += mass[i];
  }
  return output;
}

std::vector<double> load_file(char*fname) {
  std::ifstream myfile(fname, std::ifstream::binary);
  std::string line;
  std::cout.precision(10);
  std::vector<double> mz;
  if (myfile.is_open())
    {
      int ind = -1;
      while(!myfile.eof())
	{
	  ind = ind + 1;
	  getline(myfile,line);
	  std::string cleaned_mass = clean_mass(line);
	  if (cleaned_mass.size() > 0)
	    mz.push_back(std::stod(cleaned_mass));
	}
      myfile.close();
    }
  return mz;
}


int main(int argc, char* argv[]){
  if (argc<5){
    std::cout << "USAGE: <mass file> <file with organic elements> <epsilon> <max mass> " << std::endl;
    exit(1);
  }
  std::vector<double> alphabet_vector = load_file(argv[1]);
  ElementalDecomposer ed(argv[2], atof(argv[3]), atof(argv[4]));
  int alph_size = alphabet_vector.size();
  double* alphabet = new double[alph_size];
  for (int i=0; i<alphabet_vector.size(); ++i)
    alphabet[i] = alphabet_vector[i];
  
  ed.set_alphabet(alphabet, alph_size);
  ed.print_matches();

  return 0;
}
