#ifndef _ELEMENTALDECOMPOSER_HPP
#define _ELEMENTALDECOMPOSER_HPP

#include <iostream>
#include <unordered_map>
#include <string>
#include <vector>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <sstream>

#include "../mcmc/PrimitiveVector.hpp"

class ElementalDecomposer{
private:
  std::unordered_map<std::string, double> _compound_name_to_mass; // What we build on
  std::unordered_map<std::string, double> _element_name_to_mass;  // Periodic table of elements and masses
  std::unordered_map<double, std::vector<std::tuple<double, double, std::string> > >_delta_to_matches;
  double _max_mass;
  double _goal_mass;
  double _epsilon;
  PrimitiveVector<double> _alphabet;
  int _alphabet_size;
  
  void get_elements(char*fname) {
    std::string element_abbreviation, element;
    double mass;
    
    std::ifstream fin(fname);
    while (fin >> element_abbreviation >> mass >> element)
      _element_name_to_mass[element_abbreviation] = mass;
  }

  template <typename T>
  std::string to_string(const T & rhs) {
    std::string res;
    std::ostringstream ost(res);
    ost << rhs;
    return ost.str();
  }

  void sort_alphabet(){
    for (int i=0; i<_alphabet_size; ++i){
      int j=i;
      while(j > 0 && _alphabet[j-1] > _alphabet[j]){
	double tmp = _alphabet[j];
	_alphabet[j] = _alphabet[j-1];
	_alphabet[j-1] = tmp;
	--j;
      }
    }
  }

  void find_matches() {
    for (int i=0; i< _alphabet_size; ++i)
    _compound_name_to_mass[""] = 0.0;
    int alphabet_index = 0;
    for (const std::pair<std::string, double> & element_name_and_mass : _element_name_to_mass) {

      std::cerr << "Processing " << element_name_and_mass.first << std::endl;
      while(element_name_and_mass.second > _alphabet[alphabet_index] + _epsilon)
	++alphabet_index;
      
      std::unordered_map<std::string, double> new_compound_name_to_mass;
      for (const std::pair<std::string, double> & compound_name_and_mass : _compound_name_to_mass) {
        double new_compound_mass = compound_name_and_mass.second;
        int num_copies_of_element = 0;
	std::string new_compound_name = "";
	while(new_compound_mass > _alphabet[alphabet_index] + _epsilon)
	  ++alphabet_index;

        while (new_compound_mass < _goal_mass + _epsilon) {
  	  new_compound_name = compound_name_and_mass.first;
  	  if (num_copies_of_element >= 1)
  	    new_compound_name += element_name_and_mass.first;
  	  
  	  if (num_copies_of_element > 1)
  	    new_compound_name += to_string(num_copies_of_element);

  	  new_compound_name_to_mass[new_compound_name] = new_compound_mass;


	  if(alphabet_index < _alphabet_size - 1 &&  num_copies_of_element > 0 && alphabet_index < _alphabet_size && (new_compound_mass + element_name_and_mass.second) > (_alphabet[alphabet_index] + _epsilon)){
	    double err = fabs(_alphabet[alphabet_index] - new_compound_mass);
	    if (err < _epsilon)
	      _delta_to_matches[_alphabet[alphabet_index]].push_back( std::make_tuple(err, new_compound_mass, new_compound_name) );

	    ++alphabet_index;	    
	  }

  	  ++num_copies_of_element;
	  new_compound_mass += element_name_and_mass.second;
        }

	alphabet_index = 0;
      }

	_compound_name_to_mass = new_compound_name_to_mass;
    }
    //NOTE: Instead of "alphabet_index < _alphabet_size" in the above, could do this here: _delta_to_matches[_alphabet[_alphabet_size - 1]].clear(); 

    for (const std::pair<std::string, double> & compound_name_and_mass : _compound_name_to_mass) {
      double err = fabs(compound_name_and_mass.second - _alphabet[_alphabet_size - 1]);
      if (err < _epsilon){
	_delta_to_matches[_alphabet[_alphabet_size - 1]].push_back( std::make_tuple(err, compound_name_and_mass.second, compound_name_and_mass.first) );
      }
    }

        
  std::cerr << std::endl;

  }


public:
  ElementalDecomposer(char* element_file_name, double epsilon, double max_mass):
    _epsilon(epsilon),
    _alphabet_size(0),
    _max_mass(max_mass),
    _alphabet()
  {
    get_elements(element_file_name);
    _compound_name_to_mass[""] = 0.0;
  }

  ~ElementalDecomposer(){

  }

  void set_alphabet(double* alphabet, int alphabet_size){
    _alphabet_size = alphabet_size;
    for(int i=0; i<_alphabet.size(); ++i)
      _alphabet[i] = alphabet[i];

    for(int i=_alphabet.size(); i<_alphabet_size; ++i)
      _alphabet.push_back(alphabet[i]);
      
    sort_alphabet();
    int i=_alphabet_size-1;
    while(_alphabet[i] > _max_mass && i >=0)
      --i;
    
    if ( i>=0 )
      _goal_mass = _alphabet[i];
    else
      exit(1);
  }
  
  void print_matches(){
    std::cout << "ELEMENTAL DECOMPOSITION\n";
    if (_element_name_to_mass.size() == 0){
      std::cout << "No mass list for elemental decomposer.\n";
      return;
    }
    
    find_matches();
    std::cout << "MATCHES" << std::endl;
    for (int i=0; i<_alphabet_size; ++i){
      std::cout << "Delta: " << _alphabet[i] << std::endl;
        std::sort(_delta_to_matches[_alphabet[i]].begin(), _delta_to_matches[_alphabet[i]].end());
      for (const std::tuple<double, double, std::string> & m : _delta_to_matches[_alphabet[i]])
	std::cout << "\t" << std::get<0>(m) << " " << std::get<1>(m) << " " << std::get<2>(m) << std::endl;
    }
  }

  std::unordered_map<double, std::vector<std::tuple<double, double, std::string> > > get_matches(){
    find_matches();
    return _delta_to_matches;
  }
  
};

#endif
