# Copyright 2012 Oliver Serang
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

class spectrum(dict):
    def __init__(self, line_generator, percent_peak_threshold, index_in_mgf):
        self.percent_peak_threshold = percent_peak_threshold
        self.index_in_mgf = index_in_mgf
        # note: this doesn't verify the integrity of the file (via
        # finite state machine)
        for line in line_generator:
            # remove the newline
            line = line.replace('\n','')
            words = line.split()
            if len(words) == 0:
                continue
            if words[0] == '#':
                continue
            elif line.startswith('BEGIN IONS'):
                continue
            elif line.startswith('END IONS'):
                break
            elif line.startswith('TITLE='):
                self.title = line[6:]
            elif line.startswith('SCANS='):
                self.scans = line[6:]
            elif line.startswith('CHARGE='):
                self.charge = line[7:]
            elif line.startswith('PEPMASS='):
                self.pepmass = line[8:]
            elif line.startswith('RTINSECONDS='):
                self.rtinseconds = line[12:]
            else:
                mz_str, intensity_str = words[0:2]
                self[float(mz_str)] = float(intensity_str)

        if len(self) == 0:
            raise Exception('Empty spectrum loaded')

        # remove peaks below threshold
        self.max_intensity = max(self.values())
        for mz, intensity in self.items():
            if intensity < self.percent_peak_threshold * self.max_intensity:
                del self[mz]
        
        self.sorted_pairs = sorted(list(self.items()))

        #print '\tLoaded', len(self), 'peaks'

    def mz(self, i):
        return self.sorted_pairs[i][0]

    def intensity(self, i):
        return self.sorted_pairs[i][1]

    def mass_differential(self,i,j,charge):
        mz_diff = self.mz(j) - self.mz(i)
        return mz_diff * charge

class MGF:
    def __init__(self, fname, percent_peak_threshold):
        self.percent_peak_threshold = percent_peak_threshold
        self.load(fname)
    def load(self, fname):
        self.all_spectra = []
        # make readlines into a generator so that lines disappear
        # as they are read
        index_in_mgf = 0
        line_gen = ( line for line in open(fname).readlines() )
        while True:
            try:
                next_spect = spectrum(line_gen, self.percent_peak_threshold, index_in_mgf)
                index_in_mgf += 1
            except Exception:
                break
            # the spectrum was loaded successfully
            self.all_spectra.append(next_spect)
        if len(self.all_spectra) == 0:
            raise Exception('MGF file ' + fname + ' does not include any spectra')
        print 'MGF:', len(self.all_spectra), 'spectra'
