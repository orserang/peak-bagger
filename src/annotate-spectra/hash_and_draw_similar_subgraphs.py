import sys
from MGF import *
import numpy as np
from sets import Set
from matplotlib import pyplot as P
from spectra_library import *
from scipy import signal

def read_whitelist(delta_file):
  delta_to_mass = {}
  for line in open(delta_file).readlines():
    words = line.split()
    if len(words) > 1:
      delta_to_mass[ words[1] ] = float(words[0])
    elif len(words)==1:
      delta_to_mass[ str(words[0]) ] = float(words[0])
  return delta_to_mass

def set_of_random_unit_vector(number_cutting_planes, dim):
  print 'dim', dim
  unit_vectors = []
  for i in range(number_cutting_planes):
    unit_vectors.append(np.zeros(dim))
    magnitude = 0
    for j in range(dim[0]):
      unit_vectors[-1][j] = np.random.uniform(-1,1)
    mag = np.linalg.norm(unit_vectors[-1])
    for j in range(dim[0]):
      unit_vectors[-1][j] = unit_vectors[-1][j]/mag
  return unit_vectors

def hash_bit(dp):
  return (dp > 0)

def hash_spectra(set_of_unit_vectors, binned_intensities):
  fft_power_spectrum = np.abs(np.fft.fft(binned_intensities))
  fft_power_spectrum[0] = 0.0
  hash_value = 0
  bit_index = 0
  for v in set_of_unit_vectors:
    dp = np.dot(fft_power_spectrum, v)
    hash_value += hash_bit(dp) * 2**bit_index
    bit_index += 1
  return hash_value

def draw_hash_subgraphs(hash_bins, mgf_files, epsilon, delta_to_mass, discretized_mgf_list):
  #Show graph
  for key in hash_bins:
    list_of_spectra_in_bin = hash_bins[key]
    print 'Hash bin', key, 'contains', len(list_of_spectra_in_bin), 'many graphs'
    plot_dashed = True
    P.clf()
    
    if len(list_of_spectra_in_bin) >= 2:
      for i in range(0,len(list_of_spectra_in_bin),2):
        if i < len(list_of_spectra_in_bin)-1:
          mgf_and_spectrum_indices1 = list_of_spectra_in_bin[i]
          mgf_and_spectrum_indices2 = list_of_spectra_in_bin[i+1]
          
          mgf_ind1 = mgf_and_spectrum_indices1[0]
          spect_ind1 = mgf_and_spectrum_indices1[1]
          
          mgf_ind2 = mgf_and_spectrum_indices2[0]
          spect_ind2 = mgf_and_spectrum_indices2[1]

          spec1 = discretized_mgf_list[mgf_ind1][spect_ind1]
          spec2 = discretized_mgf_list[mgf_ind2][spect_ind2]
          print 'Spectrum 1\n\t Mgf index', mgf_ind1, '\n\tSpectrum index', spec1.index_in_mgf
          print 'Spectrum 2\n\t Mgf index', mgf_ind2, '\n\tSpectrum index', spec2.index_in_mgf
          last_nonzero = 0
          for j in range(min(len(spec1.binned_spectrum), len(spec2.binned_spectrum) )):
            if spec1.binned_spectrum[j] > 0 or spec2.binned_spectrum[j] > 0:
              last_nonzero = j

          max_mz_tick = last_nonzero*epsilon
          tick_step = 200
          max_mz_tick = int(max_mz_tick)

          ticks = range(0,max_mz_tick, tick_step)
          num_ticks = len(ticks)
          spec_len = max(len(spec1.binned_spectrum), len(spec2.binned_spectrum))
          tick_locations = np.arange(0,spec_len, spec_len/num_ticks)
          rolled, offset, cval = get_aligned_isomorphic_subgraph(spec1, spec2)
          if (cval > 5):
            print 'offset', offset, 'cval', cval
            shared_peaks = []
            for j in range(min(len(spec1.binned_spectrum), len(rolled) )):
              if spec1.binned_spectrum[j] > 0 and rolled[j] > 0:              
                shared_peaks.append(max(spec1.binned_spectrum[j], rolled[j]))
              else:
                shared_peaks.append(0)
            
            s1_tick_locations, s1_tick_labels = spec1.get_xticks()
            s2_tick_locations, s2_tick_labels = spec2.get_xticks()          
            alpha = 0.8
            color1 = 'blue'
            color2 = 'red'
            P.figure(5)
            P.clf()
            P.xlabel('m/z')
            P.ylabel('Intensity')
            #P.suptitle('Suspectra From Peaks Connected By Alphabet')
            
            P.xticks([], [])
            P.plot(spec1.binned_spectrum, color=color1)
            P.plot(rolled, alpha=alpha, color=color2)
            P.xlim([0,last_nonzero+3])
                
            
            P.figure(1)
            P.clf()
            
            P.xticks(s1_tick_locations, s1_tick_labels)          
            P.plot(spec1.binned_spectrum)
            P.xlim([0,last_nonzero+3])
            
            P.figure(2)
            P.clf()
            P.xlabel('m/z')
            P.ylabel('Intensity')
            #P.suptitle('Suspectra From Peaks Connected By Alphabet')
            P.xticks(s2_tick_locations, s2_tick_labels)
            P.plot(spec2.binned_spectrum)
            P.xlim([0,last_nonzero+3])
            
            P.figure(3)
            P.clf()
            P.xlabel('m/z')
            P.ylabel('Intensity')
            # P.suptitle('Shared Suspectra From Peaks Connected By Alphabet')
            P.xticks(tick_locations, ticks)
            P.plot(shared_peaks)
            P.xlim([0,last_nonzero+3])
            
            P.figure(4)
            P.clf()
            P.xlabel('m/z')
            P.ylabel('Intensity')
            #P.suptitle('Overlapping Suspectra From Peaks Connected By Alphabet')
            P.xticks([],[])
            P.plot(spec1.binned_spectrum, color=color1)
            P.plot(spec2.binned_spectrum, alpha=alpha, color=color2)
            P.xlim([0,last_nonzero+3])
            
            
            print '\tpress enter to continue, type q to quit'
            ri = raw_input()
            if ri == 'q':
              return

def cutting_planes(mgf_files, epsilon, number_cutting_planes):
  hash_bins = defaultdict(list)
  min_spectra_mass = mgf_files[0].all_spectra[0].mz(0)
  max_spectra_mass = mgf_files[0].all_spectra[0].mz(-1)
  max_span = mgf_files[0].all_spectra[0].mz(-1) - mgf_files[0].all_spectra[0].mz(0)
  for mgf in mgf_files:
    for spectra in mgf.all_spectra:
      min_spectra_mass = min(min_spectra_mass, spectra.mz(0))
      max_spectra_mass = max(max_spectra_mass, spectra.mz(-1))
      max_span = max(max_span, spectra.mz(-1) - spectra.mz(0))

  discretized_mgf_list = []
  for mgf in mgf_files:
    discretized_mgf_list.append([])
    for spectrum in mgf.all_spectra:
      discretized_mgf_list[-1].append(discretized_spectrum(spectrum, max_span, epsilon, spectrum.index_in_mgf))
    
  number_dimensions = discretized_mgf_list[0][0].binned_spectrum.shape
  set_of_unit_vectors = set_of_random_unit_vector(number_cutting_planes, number_dimensions)

  # Put spectra in bins
  print 'Hashing...'
  for i in range(len(discretized_mgf_list)):
    for j in range(len(discretized_mgf_list[i])):
      if (discretized_mgf_list[i][j].spectrum.mz(0) > -1):
        hash_bin_value = hash_spectra(set_of_unit_vectors, discretized_mgf_list[i][j].binned_spectrum)
        hash_bins[hash_bin_value].append((i,j)) # Hash knows which spectrum, j, in which mgf file, i, is hached to that bin

  return hash_bins, discretized_mgf_list
    
def main(args):
  if (len(args) < 3):
    print 'usage: <#cutting planes> <epsilon> <whitelist> <mgf 1> [mgf 2...]'
    return

  number_cutting_planes = int(args[2])
  epsilon = float(args[3])
  delta_to_mass = read_whitelist(args[4])
  mgf_files = []
  for f in args[5:]:
    mgf_files.append(MGF(f, 0.0))

  print 'hashing...'
  
  hash_bins, discretized_mgf_list = cutting_planes(mgf_files, epsilon, number_cutting_planes)
  print 'Drawing subgraphs'
  draw_hash_subgraphs(hash_bins, mgf_files, epsilon, delta_to_mass, discretized_mgf_list)  

if __name__ == '__main__':
    main(sys.argv)
