#include <vector>
#include <iostream>

#include "../mcmc/Spectrum.hpp"
#include "../mcmc/PrimitiveVector.hpp"

std::vector<Spectrum> get_spectra(int argc, char* argv[], int spectra_start_in_argv_index, double epsilon, double TABLE_EPSILON, double intensity_cutoff){
  std::vector<Spectrum> spectra;
  std::cout << "Spectra...\n";
  for (int i=spectra_start_in_argv_index; i<argc; ++i) {
    std::cout << "\t" << argv[i] << std::endl;
    std::vector<Spectrum> file_spectra = load_mgf(argv[i], epsilon, TABLE_EPSILON, intensity_cutoff);
    for (const Spectrum & s : file_spectra)
      spectra.push_back(s);

  }
  std::cout << std::endl;
  return spectra;
}
std::vector<std::pair<std::string, double> > get_whitelist(const char* whitelist_file){
  std::vector<std::pair<std::string, double> > whitelist;
  std::string line;
  std::ifstream fin(whitelist_file);
  while (getline(fin, line)) {
    if (line.empty() != 1){
      std::istringstream ist(line);
      double mass;
      std::string name;
      ist >> mass >> name;
      whitelist.push_back(std::make_pair(name, mass));
    }
  }
  return whitelist;
}


bool sort_histogram_helper(std::pair<double, double> mass_and_intensity1, std::pair<double, double> mass_and_intensity2){
  return (mass_and_intensity1.second > mass_and_intensity2.second);
}

std::pair<double, double> weighted_function(const Spectrum &s, const int i, const int j){
  return  std::make_pair(s.get_mz(j) - s.get_mz(i), s.get_log_intensity(j) * s.get_log_intensity(i));
}

std::pair<double, double> unweighted_function(const Spectrum &s, const int i, const int j){
  return  std::make_pair(s.get_mz(j) - s.get_mz(i), 1.0);
}

std::vector<std::pair<double, double> > calculate_and_print_top_histogram_values(std::vector<Spectrum> &spectra, double epsilon, const int weighted){
  std::vector<std::pair<double, double> > deltas_and_intensities;
  for (const Spectrum &s : spectra)
    for (int i=0; i < s.number_peaks(); ++i)
      for (int j=i+1; j < s.number_peaks(); ++j)
	if (weighted){
	  deltas_and_intensities.push_back(weighted_function(s, i, j));
	}
	else
	  {
	    deltas_and_intensities.push_back(unweighted_function(s, i, j));
	  }
  
  std::sort(deltas_and_intensities.begin(), deltas_and_intensities.end());
  
  int number_deltas = deltas_and_intensities.size();
  double hist_width = fabs(deltas_and_intensities[0].first - deltas_and_intensities[deltas_and_intensities.size() - 1].first);
  double bin_width = epsilon/2.0;
  long num_bins = 1 + (long)hist_width/bin_width;
  std::vector<std::pair<double, double> > histogram(num_bins);
  for (int i=0; i<num_bins; ++i){
    histogram[i].first = 0;
    histogram[i].second = 0;
  }
  int bin_counter = 0;
  int delta_counter = 0;
  double bin_value = deltas_and_intensities[0].first;
  while(bin_counter < num_bins && delta_counter < number_deltas){
    histogram[bin_counter].first = bin_value;
    while(delta_counter < number_deltas && bin_value <= deltas_and_intensities[delta_counter].first && deltas_and_intensities[delta_counter].first < bin_value + epsilon/2.0){
      histogram[bin_counter].second += deltas_and_intensities[delta_counter].second;
      ++delta_counter;
    }
    bin_value += bin_width;
    ++bin_counter;
  }

  std::sort(histogram.begin(), histogram.end(), sort_histogram_helper);
  return histogram;
}


void print_curated(const std::vector<std::pair<double, double> > &histogram, const std::vector<std::pair<std::string, double> > &whitelist, const int number_deltas_to_print, const double epsilon){
  std::cout << "Print curated histogram" << std::endl;
  int number_printed = 0;
  int i=0;
  while (i < histogram.size() && number_printed < number_deltas_to_print){ 
    bool matched_whitelist = false;
    if (histogram[i].first < .5){
      ++i;
      continue;
    }

    bool repeat = false;
    for (int j=0; j < i; ++j)
      if (fabs(histogram[j].first - histogram[i].first) < epsilon){
	repeat = true;
	break;
      }

    if (repeat){
      ++i;
      continue;
    }
    
    for (auto &whitelist_element : whitelist)
      if (fabs(whitelist_element.second - histogram[i].first) < epsilon){
	std::cout << histogram[i].first << "\t" << histogram[i].second << "\t" << whitelist_element.first << std::endl;
	matched_whitelist = true;
	break;
      }
    if (!matched_whitelist)
      std::cout << histogram[i].first << "\t" << histogram[i].second << std::endl;
    ++number_printed;
    ++i;
  }
  std::cout << std::endl;
  std::cout << std::endl;
}

void print_naive(const std::vector<std::pair<double, double> > &histogram, const std::vector<std::pair<std::string, double> > &whitelist, const int number_deltas_to_print, const double epsilon){
  std::cout << "Print naive histogram" << std::endl;
  int i = 0;
  while (i < number_deltas_to_print && i < histogram.size()){
    bool printed = false;
    for (auto delta : whitelist)
      if ( fabs(histogram[i].first - delta.second) < epsilon ){
	printed = true;
	std::cout << histogram[i].first << "\t" << histogram[i].second << "\t" << delta.first << std::endl;
	break;
      }
    if (!printed)
      std::cout << histogram[i].first << "\t" << histogram[i].second << std::endl;
    ++i;
  }
  std::cout << std::endl;
  std::cout << std::endl;
}

int main(int argc, char* argv[]){
  if (argc < 8){
    std::cout << "Usage: <naive (0) or curate output (1)> <unweighted (0) or weighted (1)> <epsilon> <intensity cutoff> <number top delta to print> <whitelist file> <mgf 1> [mgf 2,3,...]" << std::endl;
    exit(1);
  }
  int print_naive_or_curated = atoi(argv[1]);
  int weighted = atoi(argv[2]);
  double epsilon = atof(argv[3]);
  double intensity_cutoff  = atof(argv[4]);
  int number_deltas_to_print = atoi(argv[5]);
  constexpr double TABLE_EPSILON = 0.05;
  std::vector<std::pair<std::string, double> > whitelist = get_whitelist(argv[6]);
  std::vector<Spectrum> spectra =  get_spectra(argc, argv, 7, epsilon, TABLE_EPSILON, intensity_cutoff);
  std::vector<std::pair<double, double> > histogram = calculate_and_print_top_histogram_values(spectra, epsilon, weighted);
  if (print_naive_or_curated ==0)
    print_naive(histogram, whitelist, number_deltas_to_print, epsilon);
  else if (print_naive_or_curated ==1)
    print_curated(histogram, whitelist, number_deltas_to_print, epsilon);
  else{
    print_naive(histogram, whitelist, number_deltas_to_print, epsilon);
    print_curated(histogram, whitelist, number_deltas_to_print, epsilon);
  }
  return 0;
}
