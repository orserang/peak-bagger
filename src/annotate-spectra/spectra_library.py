import sys
from MGF import *
from collections import defaultdict
import numpy as np
import networkx as nx
import pylab as P
from scipy import signal  

P.ion()
charge_colors = ['red', 'green', 'blue', 'pink']

def build_graph_in_network_x(spect, delta_to_mass, possible_charges, epsilon):
  dg = nx.DiGraph()
  
  # todo: do this offline instead of as we go (better performance)
  sorted_mz = np.array([ a for a,b in spect.sorted_pairs ])

  # Note: only does neutral losses
  for z,c in zip(possible_charges, charge_colors):
    for peak_i in range(len(spect.sorted_pairs)):
      start_mz, start_intensity = spect.sorted_pairs[peak_i]
      for delta_name,delta_mass in delta_to_mass.items():
        goal_end_mz = start_mz + delta_mass/z
        
        # searchsorted finds first index after crossing goal:

        start_index = max(0, sorted_mz.searchsorted(goal_end_mz-epsilon/z)-1)
        end_index = min(len(sorted_mz)-1, sorted_mz.searchsorted(goal_end_mz+epsilon))

        for peak_j in range(start_index, end_index+1):
          end_mz = sorted_mz[peak_j]
          if np.fabs( end_mz - (start_mz + delta_mass/z) ) < epsilon:  
            dg.add_edge( start_mz, end_mz, attr_dict={'label':delta_name, 'start_and_end_indices':(peak_i,peak_j), 'color':c} )

  return dg


def build_graph(spect, delta_to_mass, possible_charges, epsilon):
  dg = nx.DiGraph()
   
  # todo: do this offline instead of as we go (better performance)
  sorted_mz = np.array([ a for a,b in spect.sorted_pairs ])

  # Note: only does neutral losses
  for z,c in zip(possible_charges, charge_colors):
    for peak_i in range(len(spect.sorted_pairs)):
      start_mz, start_intensity = spect.sorted_pairs[peak_i]
      for delta_name,delta_mass in delta_to_mass.items():
        goal_end_mz = start_mz + delta_mass/z
            
        # searchsorted finds first index after crossing goal:

        start_index = max(0, sorted_mz.searchsorted(goal_end_mz-epsilon/z)-1)
        end_index = min(len(sorted_mz)-1, sorted_mz.searchsorted(goal_end_mz+epsilon))

        for peak_j in range(start_index, end_index+1):
          end_mz = sorted_mz[peak_j]
          if np.fabs( end_mz - (start_mz + delta_mass/z) ) < epsilon:  
            dg.add_edge( start_mz, end_mz, attr_dict={'label':delta_name, 'start_and_end_indices':(peak_i,peak_j), 'color':c} )
            
      
  return dg


def draw_annotated_spectrum(spect, dg):
   P.clf()
   draw_spectrum_series(spect.items(), 'black')
   for c, series in spectrum_and_mzs_to_drawing_edges(dg, spect).items():
     draw_spectrum_series(series, c)

   P.draw()

def draw_graph_in_network_x(dg):
  P.clf()
  dg = dg.to_undirected()
  # add the m/z for the lowest m/z node

  pos = nx.spring_layout(dg)
  edges = dg.edges()
  edge_colors=[ dg[a][b]['attr_dict']['color'] for a,b in edges ]
  edge_labels=[ dg[a][b]['attr_dict']['label'] for a,b in edges ]
  
  nx.draw_networkx(dg, pos, edge_color=edge_colors)
  #nx.draw(dg, pos, edge_color=edge_colors)
  nx.draw_networkx_edge_labels(dg, pos, edge_color=edge_colors, edge_labels=dict( zip(edges, edge_labels) ) )

  ax = P.axes()
  P.ylim(ax.get_ylim()[0], ax.get_ylim()[1]*1.1)
  
  # turn off the axes
  ax.axes.get_xaxis().set_visible(False)
  ax.axes.get_yaxis().set_visible(False)
  
  P.draw()


def get_aligned_isomorphic_subgraph(d1, d2):
  f1 = np.zeros(d1.binned_spectrum.shape)
  for i in range(d1.binned_spectrum.shape[0]):
    if d1.binned_spectrum[i] >0:
      f1[i] = 1.0

  f2 = np.zeros(d2.binned_spectrum.shape)
  for i in range(d2.binned_spectrum.shape[0]):
    if d2.binned_spectrum[i] >0:
      f2[i] = 1.0
      
  convolved_spectrum = signal.fftconvolve(f1, f2[::-1])
  max_value = 0
  argmax_index = 0
  for i in range(len(convolved_spectrum)):
    if convolved_spectrum[i] > max_value:
      argmax_index = i
      max_value = convolved_spectrum[i]
  print 'convolve score', max_value
  binned_spectrum1 = d1.binned_spectrum
  return np.roll(d2.binned_spectrum, argmax_index+1), argmax_index+1, max_value

def discretize_mgf(mgf, epsilon, max_span=0):
  for s in mgf.all_spectra:
    max_span = max(s1[0][-1]-s1[0][0], s2[0][-1]-s2[0][0])
    
  num_bins = int(np.ceil((max_span) / epsilon))
  num_bins = round_num_bins_to_power_2(num_bins)
  bins = np.linspace(0, max_span+epsilon, num=num_bins)
  for s in mgf.all_spectra:
    indices = np.digitize(s.mz(len(s.mz)) - s.mz(0))

class discretized_spectrum():
  def __init__(self, spectrum, span, epsilon, index_in_mgf):
    self.spectrum = spectrum
    self.span = max(span, self.spectrum.mz(len(self.spectrum)-1) - self.spectrum.mz(0))
    self.epsilon = epsilon
    self.discretize(self.span)
    self.index_in_mgf = index_in_mgf

  def get_xticks(self):
    self.tick_locations = []
    self.tick_labels    = []
    max_mz = self.spectrum.mz(len(self.spectrum) - 1)
    min_mz = self.spectrum.mz(0)
    bins_per_mz = len(self.binned_spectrum)/(max_mz - min_mz)
    mz_per_bin  = (max_mz - min_mz)/len(self.binned_spectrum)
    step = (max_mz - min_mz) / 10.0
    step = np.ceil(step/10.0)
    step = int(step*10)
    print 'step',step
    for i in range(10):
      self.tick_locations.append(bins_per_mz * i * step)
      t = str(int(min_mz) + i*step)
      self.tick_labels.append(t)

    return self.tick_locations, self.tick_labels
  
    
  def discretize(self, span):
    self.span = max(span, self.spectrum.mz(len(self.spectrum)-1) - self.spectrum.mz(0))
    num_bins = int(np.ceil((self.span) / self.epsilon))
    num_bins = self.round_num_bins_to_power_2(num_bins)
    bins = np.linspace(0, self.span + self.epsilon, num=num_bins)
    self.indices = np.digitize([(self.spectrum.mz(i) - self.spectrum.mz(0) ) for i in range(len(self.spectrum)) ], bins)
    self.binned_spectrum = np.zeros(num_bins)
    for j in range(len(self.spectrum)):
      m = self.spectrum.mz(j) - self.spectrum.mz(0)
      i = np.exp(self.spectrum.intensity(j))
      self.binned_spectrum[self.indices[j]] = i

      
  def round_num_bins_to_power_2(self, num_bins):
    i = 1
    while(np.power(2,i) < num_bins):
      i +=1
    return np.power(2,i)
    
    
  
