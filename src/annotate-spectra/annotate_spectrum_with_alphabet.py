#!/usr/bin/python

# Copyright 2012 Oliver Serang
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import sys
from MGF import *
from collections import defaultdict
import numpy as np
import networkx as nx
import pylab as P
import pygraphviz
from networkx.drawing.nx_agraph import write_dot

P.ion()

charge_colors = ['red', 'green', 'blue', 'pink']

def draw_paired_series(series, color, lw):
    P.plot( [x for x,y in series], [y for x,y in series], color=color, lw=lw)  
  
    
def build_graph(spect, delta_to_mass, possible_charges, epsilon):
   dg = nx.DiGraph()
   
   # todo: do this offline instead of as we go (better performance)
   sorted_mz = np.array([ a for a,b in spect.sorted_pairs ])
   dot_file_edges = []
   # Note: only does neutral losses
   for z,c in zip(possible_charges, charge_colors):
     for peak_i in range(len(spect.sorted_pairs)):
         start_mz, start_intensity = spect.sorted_pairs[peak_i]
         for delta_name,delta_mass in delta_to_mass.items():
            goal_end_mz = start_mz + delta_mass/z
            
            # searchsorted finds first index after crossing goal:

            start_index = max(0, sorted_mz.searchsorted(goal_end_mz-epsilon/z)-1)
            end_index = min(len(sorted_mz)-1, sorted_mz.searchsorted(goal_end_mz+epsilon))

            for peak_j in range(start_index, end_index+1):
               end_mz = sorted_mz[peak_j]
               if np.fabs( end_mz - (start_mz + delta_mass/z) ) < epsilon:
                  lbl = delta_name
                  dot_spot = 0
                  if lbl[0] in '0123456789':
                    for lbl_i in range(len(lbl)):
                      if lbl[lbl_i] == '.':
                        dot_spot = lbl_i
                    lbl = lbl[:dot_spot+4]
                    
                  dg.add_edge( start_mz, end_mz, attr_dict={'label':lbl, 'start_and_end_indices':(peak_i,peak_j), 'color':c} )

   return dg

def log_sum(log_a,log_b):
  if log_a < log_b:
    return log_sum(log_b,log_a)
  return log_a + np.log1p( np.exp(log_b-log_a) )

def log_likelihood(dg, spect):
  result = np.NINF
  for connected_subgraph in nx.weakly_connected_component_subgraphs(dg):
    res_for_connected_graph = 0.0
    for a,b in connected_subgraph.edges():
      start_index, end_index = dg[a][b]['attr_dict']['start_and_end_indices']
      log_i1 = np.log(spect.intensity(start_index))
      log_i2 = np.log(spect.intensity(end_index))
      res_for_connected_graph += log_i1 + log_i2
      
    result = log_sum(result, res_for_connected_graph)
  return result

def draw_spectrum_series(spect_series, color='black', lw=1):
    series = []
    for mz, intensity in sorted(spect_series):
        series.extend( [ (mz,0), (mz,intensity), (mz,0) ] )
    draw_paired_series(series, color, lw)

def spectrum_and_mzs_to_drawing_edges(dg, spect):
  colors_to_series = defaultdict(set)
  for a,b in dg.edges():
    color = dg[a][b]['attr_dict']['color']
    colors_to_series[color].add( (a, spect[ a ]) )
    colors_to_series[color].add( (b, spect[ b ]) )

  return { c:list(s) for c,s in colors_to_series.items() }

def draw_annotated_spectrum(spect, dg):
   P.clf()
   draw_spectrum_series(spect.items(), 'black')
   for c, series in spectrum_and_mzs_to_drawing_edges(dg, spect).items():
     draw_spectrum_series(series, '#1f77b4')
   P.xlabel('m/z')
   P.ylabel('Intensity')

   P.draw()

def draw_graph(dg):
    P.clf()
    dg = dg.to_undirected()
    # add the m/z for the lowest m/z node

    pos = nx.spring_layout(dg)
    edges = dg.edges()
    edge_colors=[ dg[a][b]['attr_dict']['color'] for a,b in edges ]
    edge_labels=[ dg[a][b]['attr_dict']['label'] for a,b in edges ]

    nx.draw_networkx(dg, pos, edge_color=edge_colors, font_size=6, node_size=100, with_labels=True)
    #nx.draw(dg, pos, edge_color=edge_colors)
    nx.draw_networkx_edge_labels(dg, pos, font_size=6, edge_color=edge_colors, edge_labels=dict( zip(edges, edge_labels) ) )

    ax = P.axes()
    P.ylim(ax.get_ylim()[0], ax.get_ylim()[1]*1.1)

    # turn off the axes
    ax.axes.get_xaxis().set_visible(False)
    ax.axes.get_yaxis().set_visible(False)

    P.draw()

def main(args):
   if len(args) >= 4:
      error_tolerance = float(args[0])
      peak_intensity_tolerance = float(args[1])
      delta_file = args[2]

      charges = [1.,2.,3.]

      print 'epsilon =', error_tolerance
      print 'intensity cutoff =', peak_intensity_tolerance

      # get delta values from file
      delta_to_mass = {}
      for line in open(delta_file).readlines():
         words = line.split()
         if len(words) > 1:
           delta_to_mass[ words[1] ] = float(words[0])
         elif len(words)==1:
           delta_to_mass[ str(words[0]) ] = float(words[0])

      gc = 0
      for mgf_fname in args[3:]:
         print 'Processing...', mgf_fname
         my_mgf = MGF(mgf_fname, peak_intensity_tolerance)

         # for each spectrum in the MGF file
         for spect in my_mgf.all_spectra:
            dg = build_graph(spect, delta_to_mass, charges, error_tolerance)
            fn = 'graph_' + str(gc) + '.dot'
            write_dot(dg, fn)
            gc += 1
            print 'Edges:', len(dg.edges())
            print 'Subgraph edges:'
            for connected_subgraph in nx.weakly_connected_component_subgraphs(dg):
               print '\t', len(connected_subgraph.edges())
            
            delta_to_count = defaultdict(int)
            for e in dg.edges():
               delta = dg[e[0]][e[1]]['attr_dict']['label']
               delta_to_count[ delta ] += 1
            print 'Delta', '\t', 'Number uses'
            for a,b in sorted(delta_to_count.items()):
               print a, '\t', b

            print 'Log likelihod:', log_likelihood(dg, spect)

            P.figure(1)
            P.xlabel('m/z')
            P.ylabel('Intensity')
            draw_annotated_spectrum(spect, dg)
            
            P.xlabel('m/z')
            P.ylabel('Intensity')
            P.figure(2)
            draw_graph(dg)
            
            print 'press enter to continue, type q to quit'
            ri = raw_input()
            if ri == 'q':
               return
   else:
      print 'usage: <epsilon> <intensity_cutoff> <deltas> <mgf1> [mgf2...]'

if __name__ == '__main__':
    main(sys.argv[1:])
